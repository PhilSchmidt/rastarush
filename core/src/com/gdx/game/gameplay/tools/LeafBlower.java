package com.gdx.game.gameplay.tools;

import com.badlogic.gdx.graphics.Texture;
import com.gdx.game.GdxGame;
import com.gdx.game.gameplay.Player;
import com.gdx.game.gameplay.SoundManager;


public class LeafBlower extends Tool {
    private static float texOriginX = 24.0f;
    private static float texOriginY = 0.0f;
    private static float offsetX = -10.0f;
    private static float offsetY = 18.0f;
    private static Texture tex = GdxGame.ressources.get(GdxGame.ressources.imgLeafBlower, Texture.class);

    private boolean soundOn;

    public LeafBlower(Player parent) {
        super(tex, texOriginX, texOriginY, parent, offsetX, offsetY);
        maxBlowDistance = 75.0f;
        maxBlowAngle = 25.0f;
        maxBlowStrength = 7.0f;
    }

    @Override
    public void setActive(boolean active) {
        super.setActive(active);
        if (active) {
            SoundManager.loop(SoundManager.soundBlowerLoop);
            soundOn = true;
        }
        else {
            if (soundOn) {
                SoundManager.play(SoundManager.soundBlowerEnd);
                SoundManager.soundBlowerLoop.stop();
                soundOn = false;
            }
        }
    }
}

package com.gdx.game.gameplay.tools;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.GdxGame;
import com.gdx.game.gameplay.Player;
import com.gdx.game.gameplay.SoundManager;
import com.gdx.game.gameplay.emitters.FireEmitter;


public class FlameThrower extends Tool {
    private static float texOriginX = 24.0f;
    private static float texOriginY = 0.0f;
    private static float offsetX = -10.0f;
    private static float offsetY = 18.0f;
    private static Texture tex = GdxGame.ressources.get(GdxGame.ressources.imgFlamethrower, Texture.class);

    public FireEmitter fire;
    private float fireOffset = 7.0f;

    public FlameThrower(Player parent) {
        super(tex, texOriginX, texOriginY, parent, offsetX, offsetY);
        maxBlowDistance = 20.0f;
        maxBlowAngle = 20.0f;
        maxBlowStrength = 10.0f;

        fire = new FireEmitter(getFront(), parent, Vector2.Zero, 0.6f);
        fire.setParticleRate(20.0f);

        setColor(Color.RED.cpy().lerp(Color.BLACK, 0.4f));
    }

    @Override
    public void update(float delta) {
        super.update(delta);
        // fire.setPosition(getFront());
        fire.setPosition(getFront().add(getFrontVec().nor().scl(fireOffset)));
        // fire.setPosition(getBack());
        fire.update(delta);
    }

    public void draw(SpriteBatch sb) {
        super.draw(sb);
        fire.draw(sb);
    }

    @Override
    public void setActive(boolean active) {
        super.setActive(active);
        if (active) {
            fire.start();
            SoundManager.play(SoundManager.soundFlamethrowerStart);
        }
        else {
            fire.stop();
        }
    }
}

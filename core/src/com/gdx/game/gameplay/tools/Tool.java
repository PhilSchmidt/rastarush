package com.gdx.game.gameplay.tools;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.gameplay.Player;

import static java.lang.Math.abs;

public class Tool extends Sprite {
    public float angle = 0.0f;
    private float length;
    private float offsetX;
    private float offsetY;
    protected boolean visible = false;
    protected boolean active = false;
    private Player parent;

    public boolean forbidden = false;

    public float maxBlowDistance = 150.0f;
    protected float maxBlowAngle = 25.0f;
    public float maxBlowStrength = 10.0f;

    public Tool(Texture tex, float texOriginX, float texOriginY, Player parent, float offsetX, float offsetY) {
        super(tex);
        setSize(48, 48);
        this.parent = parent;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        setOrigin(texOriginX, texOriginY);
        length = getHeight();
        setPosition(parent.getX() + offsetX, parent.getY() + offsetY);
    }

    public void setAngle(float angle) {
        this.angle = angle;
        setRotation(angle);
    }

    public Vector2 getFront() {
        Vector2 front = getFrontVec();
        return new Vector2(parent.getX() + offsetX + getOriginX() + front.x,
                parent.getY() + offsetY + getOriginY() + front.y);
    }

    public Vector2 getFrontVec() {
        Vector2 front = new Vector2(0,length);
        return front.rotate(angle);
    }

    public Vector2 getBack() {
        return new Vector2(parent.getX() + offsetX + getOriginX(),
                parent.getY() + offsetY + getOriginY());
    }

    public void setVisibility(boolean visible) {
        this.visible = visible;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void update(float delta) {
        setPosition(parent.getX() + offsetX, parent.getY() + offsetY);
    }

    public void draw(SpriteBatch sb) {
        if (visible) {
            super.draw(sb);
        }
    }

    public Vector2 getBlowVec(Vector2 other) {
        if (!active) {
            return new Vector2(0, 0);
        } else {
            Vector2 front = getFront();
            Vector2 blowVec = new Vector2(other.x - front.x, other.y - front.y);
            float angleFromX = angle < 270 ? angle + 90 : angle - 270;
            if (blowVec.len() >= maxBlowDistance || abs(blowVec.angle() - angleFromX) > maxBlowAngle) {
                return new Vector2(0, 0);
            }
            float strengthFactor = (maxBlowDistance - blowVec.len()) / maxBlowDistance;
            blowVec.setLength(strengthFactor * strengthFactor * maxBlowStrength);
            return blowVec;
        }
    }
}

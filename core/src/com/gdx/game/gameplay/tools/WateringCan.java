package com.gdx.game.gameplay.tools;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.gdx.game.GdxGame;
import com.gdx.game.gameplay.Player;
import com.gdx.game.gameplay.SoundManager;
import com.gdx.game.gameplay.emitters.RainEmitter;

public class WateringCan extends Tool {
    private static float texOriginX = 24.0f;
    private static float texOriginY = 0.0f;
    private static float offsetX = -10.0f;
    private static float offsetY = 18.0f;
    private static Texture tex = GdxGame.ressources.get(GdxGame.ressources.imgWateringCan, Texture.class);

    public RainEmitter water;
    private float waterOffset = 5.0f;

    public WateringCan(Player parent) {
        super(tex, texOriginX, texOriginY, parent, offsetX, offsetY);
        maxBlowDistance = 0.0f;
        maxBlowAngle = 0.0f;
        maxBlowStrength = 0.0f;

        water = new RainEmitter(getFront());
    }

    @Override
    public void update(float delta) {
        super.update(delta);
        // fire.setPosition(getFront());
        water.setPosition(getFront().add(getFrontVec().nor().scl(waterOffset)));
        // fire.setPosition(getBack());
        water.update(delta);
    }

    public void draw(SpriteBatch sb) {
        super.draw(sb);
        water.draw(sb);
    }

    @Override
    public void setActive(boolean active) {
        super.setActive(active);
        if (active) {
            water.start();
            // SoundManager.play(SoundManager.soundFlamethrowerStart);
        }
        else {
            water.stop();
        }
    }
}

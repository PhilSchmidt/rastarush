package com.gdx.game.gameplay;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;
import com.gdx.game.GameplayScreen;
import com.gdx.game.GdxGame;
import com.gdx.game.helper.RessourcesLoader;


/**
 * Ingame-UI, draws buttons and handles gameplay-input (Keyboard, Touchscreen, Controller)
 */
public class UI implements InputProcessor, ControllerListener{

	//Reference to Screen
	private GameplayScreen gs;
	//Reference to gamelogic
	private GameControl gamecontrol;
	//Flag for hiding th UI
	private boolean hidden;
	//Pause Button
	private Sprite btnPause;
	private int buttonPauseOffset = 75;

	private BitmapFont fntCoins = GdxGame.ressources.fontPoints;
	private int fntCoinsPosX = 40;
	private int fntCoinsPosY = GdxGame.screenHeight - 10;
	private String fntCoinsTxt = "0";
	private Color currentColorCoins;

	private BitmapFont fntTime = GdxGame.ressources.fontPoints;
	private int fntTimePosX = 600;
	private int fntTimePosY = GdxGame.screenHeight - 10;
	private String fntTimeTxt = "0:00";
	private Color currentColorTime;

	//timer
	private int time;
	private int minutes;
	private int seconds;
	private String strMinutes;
	private String strSeconds;
	private float colorToggleTimerMax = 0.5f;
	private float colorToggleTimer;
	private boolean toggleColor;

	public UI(GameplayScreen screen, GameControl gamecontrol) {
		this.gs = screen;
		this.gamecontrol = gamecontrol;
		// init...
		currentColorCoins = Color.WHITE;
		currentColorTime = Color.WHITE;
		// Example: Pause Button on top right corner
		btnPause = new Sprite(new TextureRegion(GdxGame.ressources.get(GdxGame.ressources.imgButtons, Texture.class), 256, 256, 82, 86));
		btnPause.setBounds(GdxGame.screenWidth - buttonPauseOffset, GdxGame.screenHeight - buttonPauseOffset, 60, 63);
	}

	public void show() {
		this.hidden = false;
	}

	public void hide() {
		this.hidden = true;
	}
	
	public void update(float delta) {
		if (!hidden) {
			//update UI depending on GameControl values...
			fntCoinsTxt = Integer.toString(GameControl.gameScore);
			time = (int)Math.ceil(GameControl.countdown);
			minutes = (time) / 60;
			seconds = (time) % 60;
			strMinutes = Integer.toString(minutes);
			strSeconds = Integer.toString(seconds);
			strMinutes = minutes >= 10 ? strMinutes : "0" + strMinutes;
			strSeconds = seconds >= 10 ? strSeconds : "0" + strSeconds;
			fntTimeTxt = strMinutes + ":" + strSeconds;
			if (time <= 10) {
				colorToggleTimer += delta;
				if (colorToggleTimer >= colorToggleTimerMax) {
					colorToggleTimer = 0;
					toggleColor = !toggleColor;
					if (toggleColor)
						currentColorTime = Color.WHITE;
					else
						currentColorTime = Color.RED;
				}
			}
		}
	}
	
	public void draw(SpriteBatch sb) {
		if (!hidden) {

			//Draw UI....
			this.btnPause.draw(sb);
			fntCoins.setColor(currentColorCoins);
			fntCoins.draw(sb, fntCoinsTxt, fntCoinsPosX, fntCoinsPosY);
			fntTime.setColor(currentColorTime);
			fntTime.draw(sb, fntTimeTxt, fntTimePosX, fntTimePosY);
		}
	}

	private boolean checkButtonPausePressed(float sX, float sY) {
		return (sX > GdxGame.screenWidth - buttonPauseOffset && sY < buttonPauseOffset);
	}

	private void actionPause() {
		gs.loadPauseUI();
	}


	// ------------  Input Processor Methods  ------------

	@Override
	public boolean keyDown(int keycode) {
		// Keyboard controlls
		if (keycode == Keys.A) {
			World.player.goLeft();
		}
		else if (keycode == Keys.D) {
			World.player.goRight();
		}
		else if (keycode == Keys.W) {
			World.player.goUp();
		}
		else if (keycode == Keys.S) {
			World.player.goDown();
		}
		else if (keycode == Keys.L) {
			World.player.useTool();
		}
        else if (keycode == Keys.J) {
            World.player.turnLeft();
        }
        else if (keycode == Keys.K) {
            World.player.turnRight();
        }
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		// keyboard controls menu
		if (keycode == Keys.ESCAPE) {
			gs.loadPauseUI();
			//gs.dispose();
			//Gdx.app.exit();
		}
		// keyboard controls gameplay...
		else if (keycode == Keys.A) {
			World.player.stopLeft();
		}
		else if (keycode == Keys.D) {
			World.player.stopRight();
		}
		else if (keycode == Keys.W) {
			World.player.stopUp();
		}
		else if (keycode == Keys.S) {
			World.player.stopDown();
		}
		else if (keycode == Keys.L) {
			World.player.stopTool();
		}
        else if (keycode == Keys.J) {
            World.player.stopTurnLeft();
        }
        else if (keycode == Keys.K) {
            World.player.stopTurnRight();
        }

		return true;
	}


	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		float sX = screenX * GameplayScreen.ratioX;
		float sY = screenY * GameplayScreen.ratioY;

		if (GameControl.INTRO)
			GameControl.setIntroStage(GameControl.INTRO_STAGE + 1);

		//touch actions...
		if (checkButtonPausePressed(sX,sY))
			actionPause();
		else if (button == 0){
			World.player.useTool();
		}
		else if(button == 1) {
			World.player.switchTool();
		}

		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		float sX = screenX * GameplayScreen.ratioX;
		float sY = screenY * GameplayScreen.ratioY;

		if (button == 0) {
			World.player.stopTool();
		}

		return true;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		float sX = (float)screenX * GdxGame.screenWidth / Gdx.graphics.getWidth();
		float sY = (float)( Gdx.graphics.getHeight() - screenY) * GdxGame.screenHeight / Gdx.graphics.getHeight();;

		World.player.angleFromMouse(sX, sY);
		return true;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		float sX = (float)screenX * GdxGame.screenWidth / Gdx.graphics.getWidth();
		float sY = (float)( Gdx.graphics.getHeight() - screenY) * GdxGame.screenHeight / Gdx.graphics.getHeight();;

		World.player.angleFromMouse(sX, sY);
		return true;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void connected(Controller controller) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disconnected(Controller controller) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean buttonDown(Controller controller, int buttonCode) {
		if (buttonCode == 5) {
			World.player.useTool();
		}
		else if (buttonCode == 0) {
			World.player.switchTool();
		}
		return true;
	}

	@Override
	public boolean buttonUp(Controller controller, int buttonCode) {
		if (buttonCode == 5) {
			World.player.stopTool();
		}
		return true;
	}

	@Override
	public boolean axisMoved(Controller controller, int axisCode, float value) {
		float deadzoneMove = 0.2f;
		float deadzoneAngle = 0.5f;
		if (Math.abs(value) < deadzoneMove) {
			value = 0.0f;
		}

		if (axisCode == 1) {
			World.player.controllerInputX(value);
		}
		else if (axisCode == 0) {
			World.player.controllerInputY(-value);
		}
		else if (axisCode == 2 || axisCode == 3) {
            float x = -controller.getAxis(2);
            float y = -controller.getAxis(3);
            float degree = (float) (Math.atan2(y, x) * 180.0 / Math.PI);
            if (degree < 0) {
                degree += 360.0f;
            }
            if (Math.abs(x) > deadzoneAngle || Math.abs(y) > deadzoneAngle) {
                World.player.controllerToolAngle(degree);
            }
        }
		return true;
	}

	@Override
	public boolean povMoved(Controller controller, int povCode,	PovDirection value) {

		return false;
	}

	@Override
	public boolean xSliderMoved(Controller controller, int sliderCode, boolean value) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean ySliderMoved(Controller controller, int sliderCode, boolean value) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean accelerometerMoved(Controller controller, int accelerometerCode, Vector3 value) {
		return false;
	}		
}

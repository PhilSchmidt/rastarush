package com.gdx.game.gameplay;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

import java.util.ArrayList;
import java.util.Iterator;

//Da Babylon Manager
public class PoliceManager {

    public static boolean SPAWNING_ACTIVE = true;
    public static float SPAWN_INTERVAL = 8.0f;
    public static float spawn_interval = SPAWN_INTERVAL;

    public ArrayList<Policeman> policemen = new ArrayList<Policeman>();

    private Rectangle plantingAreaPosition;
    private float intervalCounter;

    public PoliceManager() {
        //initialize the position of plantation where the policemen are going to
        plantingAreaPosition = World.plantation.getWholePlantingArea();
        spawn_interval = SPAWN_INTERVAL;
    }

    public void update(float delta) {
        if (SPAWNING_ACTIVE) {
            //Spawn da police using SPAWN_INTERVAL
            spawn(delta);
        }
            //make da police update
            for (Policeman p : policemen) {
                p.update(delta);
            }
            //remove dead babylon
            Iterator<Policeman> polIter = policemen.iterator();
            while (polIter.hasNext()) {
                Policeman p = polIter.next();
                if (p.dead) {
                    polIter.remove();
                }
            }

    }

    public void draw(SpriteBatch sb) {
        //draw da police Sprites
        if (policemen != null) {
            for (Policeman p : policemen) {
                p.draw(sb);
            }
        }
    }

    private void spawn(float delta) {
        intervalCounter += delta;
        if (intervalCounter < spawn_interval)
            return;
        intervalCounter = 0.0f;

        //EXAMPLE SPAWN
        Policeman p = new Policeman();
        policemen.add(p);
    }
}

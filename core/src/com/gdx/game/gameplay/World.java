package com.gdx.game.gameplay;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.GdxGame;
import com.gdx.game.gameplay.emitters.FireEmitter;

import java.util.ArrayList;

public class World {

    public static Player player;
    public static Plantation plantation;
    public static PoliceManager policeManager;

    public static int score = 0;

    private static int numForestFires = 80;
    private static Rectangle[] forestRects;
    public static ArrayList<FireEmitter> forestFires = new ArrayList<FireEmitter>();
    public static boolean forestBurning = false;

    public World() {
        load();
    }

    private void load() {
        loadPlayer();
        loadPlantation();
        loadPolice();

        float padding = 100.0f;
        forestRects = new Rectangle[]{new Rectangle(0, 0, padding, GdxGame.screenHeight),
                new Rectangle(0, 0, GdxGame.screenWidth, padding),
                new Rectangle(0, GdxGame.screenHeight - padding, GdxGame.screenWidth, padding),
                new Rectangle(GdxGame.screenWidth - padding, 0, padding, GdxGame.screenHeight)};

        forestFires.clear();
        forestBurning = false;
        score = 0;

        for (int i = 0; i < numForestFires; i++) {
            boolean isSide = Math.random() < 0.3;
            float x = (float) Math.random();
            float y = (float) Math.random();
            if (isSide) {
                x = (x - 0.5f) * padding * 2;
                if (x < 0) {
                    x = GdxGame.screenWidth - x - 100;
                }
                else {
                    x += 20;
                }
                y *= GdxGame.screenHeight - 20;
            }
            else {
                y = (y - 0.5f) * padding * 2;
                if (y < 0) {
                    y = GdxGame.screenHeight - y - 100;
                }
                else {
                    y += 10;
                }
                x *= GdxGame.screenWidth - 10;
            }
            forestFires.add(new FireEmitter(new Vector2(x, y),null, new Vector2(0, 1.0f), 0.7f));
        }
    }

    private void loadPlantation() {
        plantation = new Plantation();
    }

    private void loadPlayer() {
        player = new Player();
    }

    private void loadPolice() {
        policeManager = new PoliceManager();
    }

    public void update(float delta) {
        plantation.update(delta);
        player.update(delta);
        policeManager.update(delta);

        resolveCollisions();

        for (FireEmitter fire: forestFires) {
            fire.update(delta);
        }
        if (forestBurning) {
            score += 113;
        }
    }

    public void resolveCollisions() {
        // check fo hungry babylon
        for (Policeman p: policeManager.policemen) {
            for (PlantationField f: plantation.fields) {
                if (f.smokeEmitter.isColliding(p.getBoundingRectangle()) > 0) {
                    p.makeHungry();
                    break;
                }
            }
            for (Policeman otherP: policeManager.policemen) {
                if (otherP.smokeEmitter.isColliding(p.getBoundingRectangle()) > 0) {
                    p.makeHungry();
                    break;
                }
            }
            if (forestBurning) {
                if (player.flameThrower.fire.isColliding(p.getBoundingRectangle()) > 5) {
                    p.burn();
                }
            }
        }
        // check fo burning fields or growing plants
        for (PlantationField f: plantation.fields) {
            int collisions = player.flameThrower.fire.isColliding(f.getRect());
            if (collisions > 0) {
                f.makeItBunDem(collisions);
            }
            collisions = player.wateringCan.water.isColliding(f.getRect());
            if (collisions > 0) {
                f.handleWater(collisions);
            }
        }
        // check for dangerous forest fires
        for (Rectangle r: forestRects) {
            if (player.flameThrower.fire.isColliding(r) > 10 && !GameControl.TUTORIAL) {
                forestBurning = true;
                for (FireEmitter fire: forestFires) {
                    fire.start();
                }
                player.flameThrower.maxBlowDistance = 80.0f;
                player.flameThrower.maxBlowStrength = 15.0f;
                player.flameThrower.fire.setParticleRate(60.0f);
                player.speedX = 300.0f;
                player.speedY = 300.0f;
                player.setWaterCanActive(false);
                player.setLeafBlowerActive(false);

                PoliceManager.spawn_interval = 1.3f;

                SoundManager.stop(SoundManager.musicIngameLoop);
                SoundManager.stop(SoundManager.musicIngameIntro);
                SoundManager.loop(SoundManager.fightLoop);
            }
        }
        // check for very nice rastaman
        for (Rectangle r: forestRects) {
            if (player.wateringCan.water.isColliding(r) > 5) {
                PoliceManager.spawn_interval = Float.MAX_VALUE;
                for (Policeman p: policeManager.policemen) {
                    p.makeHungry();
                }
            }
        }
    }

    public void draw(SpriteBatch sb) {
        plantation.draw(sb);
        policeManager.draw(sb);
        player.draw(sb);

        // draw particle systems last
        for (PlantationField f: plantation.fields) {
            f.fireEmitter.draw(sb);
            f.smokeEmitter.draw(sb);
        }
        for (FireEmitter fire: forestFires) {
            fire.draw(sb);
        }
        for (Policeman p: policeManager.policemen) {
            p.smokeEmitter.draw(sb);
            p.smokeEmitterBurn.draw(sb);
        }
    }

    public static void policeArrived() {
        //Gameover!
        //GameControl.gameover = true;
        score = 0;
        plantation.startPulseHouse(-0.3f, 0.7f, 6);
    }
}

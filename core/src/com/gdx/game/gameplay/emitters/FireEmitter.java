package com.gdx.game.gameplay.emitters;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.GdxGame;
import com.gdx.game.gameplay.Player;
import com.gdx.game.gameplay.particles.*;
import com.gdx.game.gameplay.particles.Animation;

public class FireEmitter implements EmitterI {
    private ParticleEmitter emitter;

    public FireEmitter(Vector2 position, Player player, Vector2 gravity, float scale) {
        String tex = GdxGame.ressources.imgFireParticle;
        EffectDescriptor desc = new EffectDescriptor(tex);

        float[][] sizeSupport = {{0.0f, 0.0f}, {0.2f, 1.0f * scale}, {1.0f, 0.5f * scale}};
        float[][] alphaSupport = {{0.0f, 0.0f}, {0.1f, 1.0f}, {0.4f, 1.0f}, {1.0f, 0.0f}};

        float[][] colorSupportWhite = {{0.0f, 1.0f}, {0.2f, 0.0f}, {1.0f, 0.0f}};
        float[][] colorSupportYellow = {{0.0f, 0.0f}, {0.2f, 1.0f}, {0.6f, 0.0f}, {1.0f, 0.0f}};
        float[][] colorSupportOrange = {{0.0f, 0.0f}, {0.2f, 0.0f}, {0.6f, 1.0f}, {0.8f, 0.0f}, {1.0f, 0.0f}};
        float[][] colorSupportBlack = {{0.0f, 0.0f}, {0.6f, 0.0f}, {0.8f, 1.0f}, {1.0f, 1.0f}};

        Animation[] colorAnims = {new Animation(colorSupportWhite), new Animation(colorSupportYellow),
                                  new Animation(colorSupportOrange), new Animation(colorSupportBlack)};
        Color[] colors = {Color.WHITE, Color.YELLOW, Color.ORANGE, Color.BLACK};

        desc.sizeAnim = new com.gdx.game.gameplay.particles.Animation(sizeSupport);
        desc.alphaAnim = new com.gdx.game.gameplay.particles.Animation(alphaSupport);
        desc.colorAnimation = new MultiColorAnimation(colors, colorAnims);

        desc.player = player;
        desc.drag = 0.3f;
        desc.gravity = gravity.scl(scale);
        desc.lowerSpeedThresh = 0.1f;
        desc.speedInterval[0] = 0.1f * scale;
        desc.speedInterval[1] = 0.3f * scale;
        desc.lifetime = 2.0f;
        desc.particleRate = 20.0f;
        desc.radius = 5.0f * scale;

        emitter = new ParticleEmitter(desc, position);
    }

    public void setPosition(Vector2 pos) {
        emitter.setPosition(pos);
    }

    public void start() {
        emitter.start();
    }

    public void stop() {
        emitter.stop();
    }

    public void update(float delta) {
        emitter.update(delta);
    }

    public void draw(SpriteBatch sb) {
        emitter.draw(sb);
    }

    public int isColliding(Rectangle rect) {
        return emitter.isColliding(rect);
    }

    public void setParticleRate(float rate) {
        emitter.setParticleRate(rate);
    }
}

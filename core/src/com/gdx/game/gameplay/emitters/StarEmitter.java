package com.gdx.game.gameplay.emitters;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.GdxGame;
import com.gdx.game.gameplay.emitters.EmitterI;
import com.gdx.game.gameplay.particles.Animation;
import com.gdx.game.gameplay.particles.ColorAnimation;
import com.gdx.game.gameplay.particles.EffectDescriptor;
import com.gdx.game.gameplay.particles.ParticleEmitter;

public class StarEmitter implements EmitterI {
    private ParticleEmitter emitter;

    public StarEmitter(Vector2 position) {
        String tex = GdxGame.ressources.imgStar;
        EffectDescriptor desc = new EffectDescriptor(tex);

        float[][] sizeSupport = {{0.0f, 0.0f}, {0.2f, 0.005f}, {1.0f, 0.02f}};
        float[][] alphaSupport = {{0.0f, 0.0f}, {0.1f, 1.0f}, {0.8f, 1.0f}, {1.0f, 0.0f}};
        float[][] colorSupport = {{0.0f, 0.0f}, {1.0f, 1.0f}};

        desc.sizeAnim = new Animation(sizeSupport);
        desc.alphaAnim = new Animation(alphaSupport);
        desc.colorAnimation = new ColorAnimation(Color.WHITE, Color.YELLOW, new Animation(colorSupport));

        desc.drag = 1f;
        desc.lowerSpeedThresh = 0.1f;
        desc.speedInterval[0] = 0.2f;
        desc.speedInterval[1] = 0.6f;
        desc.lifetime = 0.5f;
        desc.particleRate = 20.0f;
        desc.radius = 40.0f;

        emitter = new ParticleEmitter(desc, position);
    }

    public void start() {
        emitter.start();
    }

    public void stop() {
        emitter.stop();
    }

    public boolean isEmitting() {
        return emitter.isEmitting();
    }

    public void update(float delta) {
        emitter.update(delta);
    }

    public void draw(SpriteBatch sb) {
        emitter.draw(sb);
    }

    public int isColliding(Rectangle rect) {
        return emitter.isColliding(rect);
    }
}

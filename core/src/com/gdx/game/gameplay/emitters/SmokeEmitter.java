package com.gdx.game.gameplay.emitters;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.GdxGame;
import com.gdx.game.gameplay.Player;
import com.gdx.game.gameplay.emitters.EmitterI;
import com.gdx.game.gameplay.particles.Animation;
import com.gdx.game.gameplay.particles.ColorAnimation;
import com.gdx.game.gameplay.particles.EffectDescriptor;
import com.gdx.game.gameplay.particles.ParticleEmitter;

public class SmokeEmitter implements EmitterI {
    public ParticleEmitter emitter;

    public SmokeEmitter(Vector2 position, Player player) {
        String tex = GdxGame.ressources.imgSmokeParticle;
        EffectDescriptor desc = new EffectDescriptor(tex);

        float[][] sizeSupport = {{0.0f, 0.0f}, {0.2f, 1.0f}, {1.0f, 1.2f}};
        float[][] alphaSupport = {{0.0f, 0.0f}, {0.1f, 0.6f}, {0.8f, 0.6f}, {1.0f, 0.0f}};
        float[][] colorSupport = {{0.0f, 0.0f}, {1.0f, 1.0f}};

        desc.sizeAnim = new Animation(sizeSupport);
        desc.alphaAnim = new Animation(alphaSupport);
        desc.colorAnimation = new ColorAnimation(Color.WHITE, Color.DARK_GRAY, new Animation(colorSupport));

        desc.player = player;
        desc.drag = 0.7f;
        desc.lowerSpeedThresh = 0.1f;
        desc.speedInterval[0] = 0.2f;
        desc.speedInterval[1] = 0.6f;
        desc.lifetime = 8.0f;
        desc.particleRate = 30.0f;
        desc.radius = 20.0f;

        emitter = new ParticleEmitter(desc, position);
    }

    public void start() {
        emitter.start();
    }

    public void stop() {
        emitter.stop();
    }

    public void setPosition(Vector2 pos) {
        emitter.setPosition(pos);
    }

    public void update(float delta) {
        emitter.update(delta);
    }

    public void draw(SpriteBatch sb) {
        emitter.draw(sb);
    }

    public int isColliding(Rectangle rect) {
        return emitter.isColliding(rect);
    }

    public void setParticleRate(float rate) {
        emitter.setParticleRate(rate);
    }
}

package com.gdx.game.gameplay.emitters;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.GdxGame;
import com.gdx.game.gameplay.particles.Animation;
import com.gdx.game.gameplay.particles.ColorAnimation;
import com.gdx.game.gameplay.particles.EffectDescriptor;
import com.gdx.game.gameplay.particles.ParticleEmitter;

public class RainEmitter implements EmitterI {
    private ParticleEmitter emitter;

    public RainEmitter(Vector2 position) {
        String tex = GdxGame.ressources.imgDrop;
        EffectDescriptor desc = new EffectDescriptor(tex);

        float[][] sizeSupport = {{0.0f, 0.0f}, {0.2f, 0.3f}, {0.7f, 0.3f}, {1.0f, 0.0f}};
        float[][] alphaSupport = {{0.0f, 0.0f}, {0.15f, 1.0f}, {0.8f, 1.0f}, {1.0f, 0.0f}};
        float[][] colorSupport = {{0.0f, 0.0f}, {1.0f, 1.0f}};

        desc.sizeAnim = new Animation(sizeSupport);
        desc.alphaAnim = new Animation(alphaSupport);
        Color color = new Color(0.4f, 0.5f, 1.0f, 1.0f);
        desc.colorAnimation = new ColorAnimation(color, color, new Animation(colorSupport));

        desc.player = null;
        desc.drag = 1.0f;
        desc.lowerSpeedThresh = 0.0f;
        desc.speedInterval[0] = 0.2f;
        desc.speedInterval[1] = 0.4f;
        desc.lifetime = 0.7f;
        desc.particleRate = 15.0f;
        desc.radius = 2.5f;

        desc.gravity = new Vector2(0, -2.5f);

        emitter = new ParticleEmitter(desc, position);
    }

    public void setPosition(Vector2 pos) {
        emitter.setPosition(pos);
    }

    @Override
    public void start() {
        emitter.start();
    }

    @Override
    public void stop() {
        emitter.stop();
    }

    @Override
    public void update(float delta) {
        emitter.update(delta);
    }

    @Override
    public void draw(SpriteBatch sb) {
        emitter.draw(sb);
    }

    @Override
    public int isColliding(Rectangle rect) {
        return emitter.isColliding(rect);
    }
}

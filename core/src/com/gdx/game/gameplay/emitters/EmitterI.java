package com.gdx.game.gameplay.emitters;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public interface EmitterI {
    public void start();
    public void stop();
    public void update(float delta);
    public void draw(SpriteBatch sb);
    public int isColliding(Rectangle rect);
}

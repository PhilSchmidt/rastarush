
package com.gdx.game.gameplay;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Align;
import com.gdx.game.GdxGame;

public class SpeechBubble {
    private float targetWidth = 200.0f;
    private int padding = 8;
    private BitmapFont font = GdxGame.ressources.fontSpeechBubbles;
    private Color fontColor = new Color(0.0f, 0.0f, 0.0f, 1.0f);
    private GlyphLayout layout;
    private Sprite whiteRect = new Sprite(GdxGame.ressources.get(GdxGame.ressources.whiteRect, Texture.class));
    private Sprite triangle = new Sprite(GdxGame.ressources.get(GdxGame.ressources.triangle, Texture.class));
    private float offsetX;
    private float offsetY = 25;
    private float posX = 0;
    private float posY = 0;
    private float smallestXRun = Float.POSITIVE_INFINITY;

    public SpeechBubble(String msg) {
        layout = new GlyphLayout(font, msg, fontColor, targetWidth, Align.center, true);
        whiteRect.setSize(layout.width + 2 * padding, layout.height + 2 * padding);
        offsetX = -layout.width / 2;

        for (GlyphLayout.GlyphRun r: layout.runs) {
            smallestXRun = java.lang.Math.min(smallestXRun, r.x);
        }
    }

    public void update(float posX, float posY) {
        this.posX = posX + offsetX - smallestXRun;
        this.posY = posY + offsetY + layout.height;
        whiteRect.setPosition(posX + offsetX - padding, posY + offsetY - padding);
        triangle.setPosition(posX - triangle.getWidth() / 2 + 70, posY);
    }

    public void draw(SpriteBatch sb) {
        triangle.draw(sb);
        whiteRect.draw(sb);
        font.draw(sb, layout, posX, posY);
    }
}

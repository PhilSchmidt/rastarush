package com.gdx.game.gameplay;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class IngameFont {

	BitmapFont font;
	String text;
	float x;
	float y;
	float r;
	float g;
	float b;
	float alpha = 1.0f;
	float time;
	boolean destroyed;
	
	public IngameFont(BitmapFont font, String text, float x, float y, float time, float r, float g, float b) {
		this.text = text;
		this.x = x;
		this.y = y;
		this.r = r;
		this.g = g;
		this.b = b;
		this.time = time;
		this.font = font;// new BitmapFont(Gdx.files.internal(GdxGame.fontPath), false); //Nicht da jedesmal Ruckler beim erzeugen von IngameFont
		//font.setColor(r, g, b, 1);
		
		//font.getData().scale(-0.1f);
	}
	
	public void update(float delta) {
		//Move font upwards and destroy it
		if (!destroyed) {
			time -= delta;
			if (time > 0) {
				y += delta * 150;
				x -= delta * 150;
				//this.alpha -= (delta*0.8f);
				//font.setColor(this.r, this.g, this.b, this.alpha);
			} else {
				this.destroyed = true;
			}
		}
	}
	
	public void draw(SpriteBatch sb) {
		this.font.draw(sb, text, x, y);
	}
}

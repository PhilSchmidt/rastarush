package com.gdx.game.gameplay.particles;

import com.badlogic.gdx.graphics.Color;

public class ColorAnimation implements ColorAnimationI {
    private Animation animation;
    private Color color0;
    private Color color1;

    public ColorAnimation(Color color0, Color color1, Animation anim) {
        this.color0 = color0;
        this.color1 = color1;
        this.animation = anim;
    }

    public Color get(float x) {
        Color color3 = color0.cpy();
        color3.lerp(color1, animation.get(x));
        return color3;
    }
}

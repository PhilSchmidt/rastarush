package com.gdx.game.gameplay.particles;

public class Animation {
    private float[][] linearTerms;
    private float[] intervals;

    public Animation(float[][] support) {
        // knot vector
        intervals = new float[support.length - 1];
        for (int i = 1; i < support.length; i++) {
            intervals[i - 1] = support[i][0];
        }
        // cheap method (0th order spline)
        linearTerms = new float[support.length - 1][4];
        for (int i = 1; i < support.length; i++) {
            linearTerms[i-1][0] = 0;
            linearTerms[i-1][1] = 0;
            linearTerms[i-1][2] = (support[i][1] - support[i-1][1]) / (support[i][0] - support[i-1][0]);
            linearTerms[i-1][3] = support[i][1] - linearTerms[i-1][2] * support[i][0];
        }
    }

    public float get(float x) {
        for (int i = 0; i < intervals.length; i++) {
            if (x <= intervals[i]) {
                float[] terms = linearTerms[i];
                return terms[0]*x*x*x + terms[1]*x*x + terms[2]*x + terms[3];
            }
        }
        return 0.0f;
    }
}

package com.gdx.game.gameplay.particles;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.Iterator;

public class ParticleEmitter {
    public EffectDescriptor effectDescriptor;
    private Vector2 position;
    private boolean emitting = false;
    private ParticlePool pool;
    private ArrayList<Particle> particles = new ArrayList<Particle>();

    public ParticleEmitter(EffectDescriptor desc, Vector2 position) {
        this.effectDescriptor = desc;
        this.position = position;
        pool = new ParticlePool(desc, (int) (desc.particleRate * desc.lifetime));
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public void start() {
        emitting = true;
    }

    public void stop() {
        emitting = false;
    }

    public boolean isEmitting() {
        return emitting;
    }

    public int isColliding(Rectangle rect) {
        int collisions = 0;
        for (Particle p: particles) {
            if (rect.contains(p.getPos())) {
                collisions++;
            }
        }
        return collisions;
    }

    public void update(float delta) {
        // Update old particles an delete eff necessary
        Iterator<Particle> it = particles.iterator();
        while (it.hasNext()) {
            Particle p = it.next();
            if (!p.update(delta)) {
                p.discard();
                it.remove();
            }
        }

        // smoke da weed
        if (emitting) {
            float expectedParticles = effectDescriptor.particleRate * delta;
            if (Math.random() < expectedParticles) {
                Particle p = pool.acquire();
                Vector2 offset = new Vector2(1,0);
                offset.setToRandomDirection();
                offset.setLength((float) (Math.random() * effectDescriptor.radius));
                p.init(position.cpy().add(offset));
                particles.add(p);
            }
        }
    }

    public void draw(SpriteBatch sb) {
        for (Particle p: particles) {
            p.draw(sb);
        }
    }

    public void setParticleRate(float rate) {
        effectDescriptor.particleRate = rate;
    }
}

package com.gdx.game.gameplay.particles;

import com.badlogic.gdx.graphics.Color;

public interface ColorAnimationI {
    public Color get(float x);
}

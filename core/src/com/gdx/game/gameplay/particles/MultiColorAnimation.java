package com.gdx.game.gameplay.particles;

import com.badlogic.gdx.graphics.Color;

public class MultiColorAnimation implements ColorAnimationI {
    private Color[] colors;
    private Animation[] anims;

    public MultiColorAnimation(Color[] colors, Animation[] anims) {
        this.colors = colors;
        this.anims = anims;
    }

    public Color get(float x) {
        Color result = Color.BLACK.cpy();
        Color intermediate;
        for (int i = 0; i < colors.length; i++) {
            intermediate = colors[i].cpy();
            intermediate.mul(anims[i].get(x));
            result.add(intermediate);
        }
        result.clamp();
        return result;
    }
}

package com.gdx.game.gameplay.particles;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.GdxGame;

public class Particle extends Sprite {
    private EffectDescriptor effectDescriptor;
    private ParticlePool parentPool;

    private float age = 0.0f; // in percent
    private Vector2 speed = new Vector2(0, 0);
    private Vector2 pos = new Vector2(0, 0);

    public Particle(EffectDescriptor effectDescriptor) {
        super(GdxGame.ressources.get(effectDescriptor.texturePath, Texture.class));
        setOriginCenter();
        this.effectDescriptor = effectDescriptor;

        setPosition(pos.x - getWidth() / 2, pos.y - getHeight() / 2);
        setScale(effectDescriptor.sizeAnim.get(age));
        setColor(effectDescriptor.colorAnimation.get(age));
        setAlpha(effectDescriptor.alphaAnim.get(age));
        setRotation(effectDescriptor.rotationAnim.get(age));
    }

    // return false if over lifetime
    public boolean update(float delta) {
        age += delta / effectDescriptor.lifetime;
        if (age >= 1) {
            return false;
        }

        Vector2 blowVec;
        if (effectDescriptor.player != null) {
            blowVec = effectDescriptor.player.getBlowVec(pos);
        }
        else {
            blowVec = new Vector2(0, 0);
        }

        speed.add(blowVec.scl(delta));
        Vector2 gravity = effectDescriptor.gravity.cpy();
        speed.add(gravity.scl(delta));
        speed.scl(1.0f - delta * effectDescriptor.drag);
        if (speed.len() < effectDescriptor.lowerSpeedThresh && blowVec.len() == 0) {
            speed.setZero();
        }
        pos.add(speed);

        setPosition(pos.x - getWidth() / 2, pos.y - getHeight() / 2);
        setScale(effectDescriptor.sizeAnim.get(age));
        setColor(effectDescriptor.colorAnimation.get(age));
        setAlpha(effectDescriptor.alphaAnim.get(age));
        setRotation(effectDescriptor.rotationAnim.get(age));

        return true;
    }

    public void draw(SpriteBatch sb) {
        super.draw(sb);
    }

    public Vector2 getPos() {
        return pos;
    }

    public void init(Vector2 pos) {
        age = 0.0f;

        this.pos = pos;
        setPosition(pos.x, pos.y);

        speed.setToRandomDirection();
        speed.setLength((float) (effectDescriptor.speedInterval[0] + Math.random() * (effectDescriptor.speedInterval[1]
                - effectDescriptor.speedInterval[0])));
    }

    public void setParentPool(ParticlePool parent) {
        parentPool = parent;
    }

    public void discard() {
        if (parentPool != null) {
            parentPool.discard(this);
        }
    }
}

package com.gdx.game.gameplay.particles;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.gameplay.Player;

public class EffectDescriptor {
    // emitter attributes
    public float[] speedInterval = {0.2f, 0.4f};
    public float particleRate = 20.0f;
    public float radius = 1.0f;

    // particle attributes
    public String texturePath;
    public float lifetime = 5.0f;
    public Animation sizeAnim;
    public Animation alphaAnim;
    public Animation rotationAnim;
    public ColorAnimationI colorAnimation;

    public float drag = 0.0f;
    public Vector2 gravity = new Vector2(0, 0);
    public float lowerSpeedThresh = 0.1f;
    public Player player;


    public EffectDescriptor(String particleTexPath) {
        texturePath = particleTexPath;

        float[][] constantOne = {{0.0f, 1.0f}, {1.0f, 1.0f}};
        float[][] constantZero = {{0.0f, 0.0f}, {1.0f, 0.0f}};
        Animation defaultAnimOne = new Animation(constantOne);
        Animation defaultAnimZero = new Animation(constantZero);
        sizeAnim = defaultAnimOne;
        alphaAnim = defaultAnimOne;
        rotationAnim = defaultAnimZero;
        colorAnimation = new ColorAnimation(Color.WHITE, Color.WHITE, defaultAnimOne);
    }
}

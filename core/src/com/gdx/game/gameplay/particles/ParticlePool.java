package com.gdx.game.gameplay.particles;

import java.util.Stack;

public class ParticlePool {
    private EffectDescriptor effectDescriptor;
    private Stack<Particle> pool = new Stack<Particle>();

    public ParticlePool(EffectDescriptor effectDesc, int initialPoolSize) {
        this.effectDescriptor = effectDesc;
        for (int i = 0; i < initialPoolSize; i++) {
            pool.add(new Particle(effectDesc));
        }
    }

    public Particle acquire() {
        if (pool.size() == 0) {
            Particle p = new Particle(effectDescriptor);
            p.setParentPool(this);
            return p;
        }
        return pool.pop();
    }

    public void discard(Particle particle) {
        pool.push(particle);
    }
}

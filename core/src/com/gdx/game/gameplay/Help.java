package com.gdx.game.gameplay;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.gdx.game.GdxGame;


//Guy showing up in a corner, giving some hints

public class Help extends AnimatedSprite {


    private String text;
    private SpeechBubble bubble;
    private float speechBubbleTime=8;
    public float animationTime;
    public double birdScale;
    public int animationFrequency=3;

    private boolean speechBubbleVisible=true;
    private boolean birdVisible=true;

    private int birdWidth = 140;
    private int birdHeight = 150;

    private int soundNr=0;

    private String textureBird = GdxGame.ressources.imgBird;
    //private String textureSpeechBubble=GdxGame.ressources.imgSpeechbubble;

    private int birdPosX = GdxGame.screenWidth*10/10 - birdWidth;
    private int birdPosY = GdxGame.screenHeight*1/100;// - birdHeight / 2;

    //private int bubblePosX=GdxGame.screenWidth*9/10 - birdWidth / 2;;
    //private int bubblePosY=GdxGame.screenHeight*1/20;// - birdWidth / 2;;


    public Help() {
        //load halp
        super();
        addAnimation(GdxGame.ressources.get(textureBird, Texture.class), 1, 1, 0, 1, 1, "bird", false);
        setAnimation("bird");
        setPosition(birdPosX, birdPosY);
        setSize(birdWidth, birdHeight);
        setOriginCenter();
    }

    //Calling dem wi set di visibility of bud an speech bubble
    public void setBirdVisible(boolean bird) {
        if (bird==true){
            //Zwitschern Begrüßung
            SoundManager.play(SoundManager.soundBird1);
            animationTime=0;
        }
        birdVisible=bird;

    }
    public void setBubbleVisible(boolean bubble){
        speechBubbleVisible=bubble;
        if (bubble==true) {
            switch (soundNr){
            case 0:
                SoundManager.play(SoundManager.soundBird1);
                break;
            case 1:
                SoundManager.play(SoundManager.soundBird2);
                break;
            case 2:
                SoundManager.play(SoundManager.soundBird3);
                break;
            default:
                SoundManager.play(SoundManager.soundBird1);
            }

            soundNr+=1;
            if (soundNr>=3){
                soundNr=0;
            }
            animationTime=0;
        }
    }



    //Calling dem yah wi set which text to display
    public void setIntro1(float tm){
        text="Aalrait! Whá gwann?\n" +
                "Everyt’ing irie?\n" +
                "Yuh di Fawma, aint ya? ";
        bubble = new SpeechBubble(text);
        speechBubbleTime=tm;
        setBubbleVisible(true);
    }
    public void setIntro2(float tm){
        text="Mi see dem babylon cops wanna raid yuh yard!";
        bubble = new SpeechBubble(text);
        speechBubbleTime=tm;
        setBubbleVisible(true);
    }
    public void setIntro3(float tm){
        text="But no Fawma, nuh worry!";
        bubble = new SpeechBubble(text);
        speechBubbleTime=tm;
        setBubbleVisible(true);

    }
    public void setIntro4(float tm){
        text="Dem Babylon cops be esily distracted, wen yuh a go \n" +
                "burn dem plants down. Dem smellin, dem gettin hungry, \n" +
                "dem wantin eat doughnuts instead, dem go away. ";
        bubble = new SpeechBubble(text);
        speechBubbleTime=tm;
        setBubbleVisible(true);
    }

    public void setTutorial1(float tm){
        text="Left click wid yuh watering pot fi water dem seeds. Yuh new jahponese maple " +
                "gonna be sprouting soon";
        bubble = new SpeechBubble(text);
        speechBubbleTime=tm;
        setBubbleVisible(true);

    }

    public void setTutorial2(float tm){
        text="Harvest field when dem seeds be grown up";
        bubble = new SpeechBubble(text);
        speechBubbleTime=tm;
        setBubbleVisible(true);
    }

    public void setTutorial3a(float tm){
        text="Irie! Now switch yuh tool to di flamethrowa with a rightclick";
        bubble = new SpeechBubble(text);
        speechBubbleTime=tm;
        setBubbleVisible(true);
    }

    public void setTutorial3b(float tm) {
        text = "Use di flamethrowa wid yuh left mouse button.  Point yuh mouse at di field yuh wa fi bun";
        bubble = new SpeechBubble(text);
        speechBubbleTime = tm;
        setBubbleVisible(true);
    }

    public void setTutorial4a(float tm){
        text="Nice! Now select yuh leafblowah wid a right click";
        bubble = new SpeechBubble(text);
        speechBubbleTime=tm;
        setBubbleVisible(true);

    }
    public void setTutorial4b(float tm) {
        text = "Den blow di steam inna di cop's face wid yuh left mouse button.";
        bubble = new SpeechBubble(text);
        speechBubbleTime = tm;
        setBubbleVisible(true);
    }

    public void setTutorial5(float tm){
        text="Up up! Well dun. Cops be cops... Now repair burned seeds with da watercan.";
        bubble = new SpeechBubble(text);
        speechBubbleTime=tm;
        setBubbleVisible(true);
    }

    public void setTutorialRetry(float tm){
        text="Oh no, yuh field went out! Plant new one and burn again!";
        bubble = new SpeechBubble(text);
        speechBubbleTime=tm;
        setBubbleVisible(true);
    }

    public void setTutorialEnd(float tm){
        text="Nice! Now have fun! An dun let dem cops steal yuh harvest! Get as much" +
                "as yuh can!";
        bubble = new SpeechBubble(text);
        speechBubbleTime=tm;
        setBubbleVisible(true);
    }

    public void warnForestFire(float tm){
        text="An rememba nuh fi burn di forest! Rely, nuh do it! Di wood is highly flammable!! Be really careful!";
        bubble = new SpeechBubble(text);
        speechBubbleTime=tm;
        setBubbleVisible(true);
    }


    public void update(float delta) {
        if (animationTime<=10){
            animationTime+=delta;
        }
        if (animationTime<=2){
            birdScale=1+0.1f*Math.sin(2*Math.PI*animationFrequency*animationTime)*Math.exp(-animationTime);
        }
        else{
            birdScale=1;
        }
        if ((animationTime>=speechBubbleTime)&&(speechBubbleTime!=0)){
            setBubbleVisible(false);
        }
        setScale((float)birdScale);
        super.animate(delta);
        if (speechBubbleVisible){
            bubble.update(GdxGame.screenWidth*9/10 - birdWidth / 4,GdxGame.screenHeight*1/8);
        }

    }

    public void draw(SpriteBatch sb) {

        if (birdVisible){
            //draw bird with birdScale if set to true
            super.draw(sb);
        }

        if (speechBubbleVisible){
            //draw speech bubble if set to true
            bubble.draw(sb);

        }


    }


    /*
    public void draw(SpriteBatch sb) {
        // do da drawing ting
        bird.draw(sb);
    }
    */

}

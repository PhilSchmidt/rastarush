package com.gdx.game.gameplay;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.gdx.game.GdxGame;

import java.util.ArrayList;

import java.awt.*;

//Manages growing of da ganja


public class Plantation {

    public Sprite house = new Sprite(GdxGame.ressources.get( GdxGame.ressources.imgHouse, Texture.class));
    public Sprite threeLittleBirds = new Sprite(GdxGame.ressources.get( GdxGame.ressources.imgThreeLittleBirds, Texture.class));

    private int plantationWidth = 620;
    private int plantationHeight = 320;
    private int xPos = (GdxGame.screenWidth / 2) -  (plantationWidth / 2);
    private int yPos = (GdxGame.screenHeight / 2) -  (plantationHeight / 2);

    public ArrayList<PlantationField> fields = new ArrayList<PlantationField>();
    public Rectangle wholePlantingArea;
    private Sprite background;
    private int plantHeight = 100;
    private int plantWidth = 80;
    private int plantYOffset = 50;
    private int plantXOffset = 100;
    private int plantSpacing = 100;

    private boolean isPulsing = false;
    private float pulseDuration;
    private float pulseTime;
    private float pulseAmplitude;
    private float pulseTimeFactor; // for sin function

    private float houseScaleNormal = 1.25f;

    public Plantation() {
        //init planting areas
        setWholePlantingArea(xPos, yPos, plantationWidth, plantationHeight);//Example
        //setPlantationFieldArea(xPos, yPos, plantationWidth, plantation Height);
        //setWholePlantingArea()
        background = new Sprite(GdxGame.ressources.get(GdxGame.ressources.imgBackground, Texture.class));
        background.setSize(GdxGame.screenWidth, GdxGame.screenHeight);
        house.setPosition(GdxGame.screenWidth / 2 - house.getWidth() / 2, GdxGame.screenHeight / 2 - house.getHeight() / 2);
        house.setOriginCenter();
        house.setScale(houseScaleNormal);
        threeLittleBirds.setPosition(GdxGame.screenWidth / 2 - threeLittleBirds.getWidth() / 2+5, GdxGame.screenHeight / 2 - threeLittleBirds.getHeight() / 2-house.getHeight()/6);
        threeLittleBirds.setScale(0.075f);
        threeLittleBirds.setOriginCenter();


        for (int i = 0; i < 2; i++) { //top right
            float a = (float) GdxGame.screenWidth/2 + plantXOffset + plantSpacing*i;
            float b = (float) GdxGame.screenHeight/2 + plantYOffset;
            fields.add(new PlantationField(this, a, b));
        }
        for (int i = 0; i < 2; i++) { //top left
            float a = (float) GdxGame.screenWidth/2 - plantWidth - plantXOffset - plantSpacing*i;
            float b = (float) GdxGame.screenHeight/2 + plantYOffset;
            fields.add(new PlantationField(this, a, b));
        }
        for (int i = 0; i < 2; i++) { //bottom left
            float a = (float) GdxGame.screenWidth/2  - plantWidth - plantXOffset - plantSpacing*i;
            float b = (float) GdxGame.screenHeight/2 - plantHeight - plantYOffset;
            fields.add(new PlantationField(this, a, b));
        }
        for (int i = 0; i < 2; i++) { //bottom right
            float a = (float) GdxGame.screenWidth/2 + plantXOffset + plantSpacing*i;
            float b = (float) GdxGame.screenHeight/2 - plantHeight - plantYOffset;
            fields.add(new PlantationField(this, a, b));
        }
    }

    public void update(float delta) {
        //make da ganja grow
        for (PlantationField f: fields) {
            f.update(delta);
        }

        if (isPulsing) {
            pulseHouse(delta);
        }
    }

    public void draw(SpriteBatch sb) {
        //show da ganja plantation
        background.draw(sb);

        for (PlantationField f: fields) {
            f.draw(sb);
        }

        house.draw(sb);
        threeLittleBirds.draw(sb);
    }

    public void setWholePlantingArea(int x, int y, int width, int height) {
        wholePlantingArea = new Rectangle(x, y, width, height);
    }

    public Rectangle getWholePlantingArea() {
        return wholePlantingArea;
    }

    public void startPulseHouse(float amplitude, float time, int cycles) {
        if (isPulsing) {
            return;
        }
        pulseAmplitude = amplitude;
        pulseDuration = time;
        pulseTime = 0;
        pulseTimeFactor = (float) ((Math.PI * 2 * cycles) / time) * 0.9f; // don't ask for 0.9
        isPulsing = true;
    }

    private void pulseHouse(float delta) {
        pulseTime += delta;
        if (pulseTime > pulseDuration) {
            isPulsing = false;
            house.setScale(houseScaleNormal);
            return;
        }
        float scale = (float) (houseScaleNormal + (0.5 * Math.sin(pulseTimeFactor * pulseTime) + 0.5) * pulseAmplitude);
        house.setScale(scale);
    }
}

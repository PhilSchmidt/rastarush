package com.gdx.game.gameplay;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public class SoundManager {

    public static boolean SOUND_ON = true;

    public static Music musicIngameIntro = Gdx.audio.newMusic(Gdx.files.internal("sound/Reggae_long_once.ogg"));
    public static Music musicIngameLoop = Gdx.audio.newMusic(Gdx.files.internal("sound/Reggae_long_loop.ogg"));
    public static Music musicMenuIntro = Gdx.audio.newMusic(Gdx.files.internal("sound/Reggae_short_once.ogg"));
    public static Music musicMenuLoop = Gdx.audio.newMusic(Gdx.files.internal("sound/Reggae_short_loop.ogg"));
    public static Music fightLoop = Gdx.audio.newMusic(Gdx.files.internal("sound/fight_looped.ogg"));


    public static Sound soundPoliceHigh = Gdx.audio.newSound(Gdx.files.internal("sound/sfx/laugh.mp3"));
    public static Sound soundPoliceSpawn = Gdx.audio.newSound(Gdx.files.internal("sound/sfx/police_scanner.mp3"));

    public static Sound soundBlowerLoop = Gdx.audio.newSound(Gdx.files.internal("sound/sfx/LaubgeblaseLoop.ogg"));
    public static Sound soundBlowerEnd = Gdx.audio.newSound(Gdx.files.internal("sound/sfx/GeblaseAUS.ogg"));

    public static Sound soundFlamethrowerStart = Gdx.audio.newSound(Gdx.files.internal("sound/sfx/Flammenwerfer.mp3"));

    public static Sound soundBird1 = Gdx.audio.newSound(Gdx.files.internal("sound/sfx/birdhey.mp3"));
    public static Sound soundBird2 = Gdx.audio.newSound(Gdx.files.internal("sound/sfx/bird_song_1.mp3"));
    public static Sound soundBird3 = Gdx.audio.newSound(Gdx.files.internal("sound/sfx/birdrechts.mp3"));

    public SoundManager() {

    }

    public static void play(Sound sound) {
        if (SOUND_ON)
            sound.play();
    }

    public static void play(Sound sound, float volume) {
        if (SOUND_ON)
            sound.play(volume);
    }

    public static void play(Music m) {
        if (SOUND_ON)
            m.play();
    }

    public static void play(Music m, float volume) {
        if (SOUND_ON) {
            m.play();
            m.setVolume(volume);
        }
    }

    public static void stop(Music m) {
        m.stop();
    }

    public static void loop(Sound sound) {
        if (SOUND_ON)
            sound.loop();
    }

    public static void loop(Sound sound, float volume) {
        if (SOUND_ON)
            sound.loop(volume);
    }

    public static void loop(Music m) {
        if (SOUND_ON) {
            m.setLooping(true);
            m.play();
        }
    }

    public static void loop(Music m, float volume) {
        if (SOUND_ON) {
            m.setVolume(volume);
            loop(m);
        }
    }
}

package com.gdx.game.gameplay;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.*;
import com.gdx.game.GameplayScreen;

/**
 * Steuert den Spielvorgang
 * @author Philip
 *
 */
public class GameControl  {

    public static boolean TUTORIAL = true;
    public static int TUTORIAL_STAGE = -1;

    public static boolean INTRO = true;
	public static int INTRO_STAGE = -1;

	public final float COUNTDOWN_MAX = 180;
	public static boolean HELP_ACTIVE = true;
	public static Help help;

	public static boolean gameover;
	public static int gameScore;
	public static float countdown;

	/**
	 * Map mit Player-Eigenschaften (Strings) und ihren Werten (Ints)
	 */
	private Map<String, Integer> playerSettings;

	/**
	 * Map mit Welt-Eigenschaften (Strings) und ihren Werten (Ints)
	 */
	private Map<String, Integer> worldSettings;

	private boolean started;
	private boolean pause;

	private World world;

	public GameControl() {
	}

	public boolean isGameover() {
		return gameover;
	}

	public int getPoints() {
		return this.gameScore;
	}

	public HashMap<String, Integer> getGameStats() {
		HashMap<String, Integer> stats = new HashMap<String, Integer>();

		stats.put("points", this.getPoints());
		//...
		return stats;
	}

	public boolean load(Map<String, Integer> worldSettings, Map<String, Integer> playerSettings) {
		this.playerSettings = playerSettings;

		countdown = COUNTDOWN_MAX;

		this.world = new World();

		help = new Help();

		if (GameplayScreen.PLAYED_ONE_TIME) {
			TUTORIAL = false;
		}
		INTRO = true;
		PoliceManager.SPAWNING_ACTIVE = true;
		Policeman.speedX = Policeman.speedXMax;

		started = false;

		SoundManager.stop(SoundManager.fightLoop);

		return true;
	}

	public void start() {
		SoundManager.stop(SoundManager.musicMenuLoop);
		SoundManager.stop(SoundManager.musicMenuIntro);
		SoundManager.play(SoundManager.musicIngameIntro);
		help.setIntro1(5);
		this.pause = false;
		this.started = true;
	}

	public void update(float delta) {
		if (started && !pause) {
			//update
            if (!TUTORIAL) {
                countdown -= delta;
                if (countdown <= 0) {
                    gameover = true;
                    countdown = 0;
                }
            }

			if (INTRO && INTRO_STAGE == -1) {
				setIntroStage(0);
			}
            if (!INTRO && TUTORIAL && TUTORIAL_STAGE == -1)
                setTutorialStage(0);

			this.world.update(delta);
			gameScore = World.score;

			if (HELP_ACTIVE)
				help.update(delta);

			if (!SoundManager.musicIngameIntro.isPlaying() && !SoundManager.musicIngameLoop.isPlaying() && !World.forestBurning)
				SoundManager.play(SoundManager.musicIngameLoop);
		}
	}

	public void draw(SpriteBatch sb) {
		Gdx.gl.glClearColor(0.3f, 0.5f, 0.7f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		this.world.draw(sb);

		if (HELP_ACTIVE)
			help.draw(sb);
	}

	public void pause() {
		pause = true;
	}

	public void resume() {
		pause = false;
	}

	public static void setIntroStage(int stage) {
		INTRO_STAGE = stage;
		switch (stage) {
			case 0:
				if (TUTORIAL) {
					PoliceManager.SPAWNING_ACTIVE = false;
					World.player.setFlamethrowerActive(false);
					World.player.setLeafBlowerActive(false);
				}
				help.setIntro1(0);
				break;
			case 1:
				help.setIntro2(0);
				break;
			case 2:
				help.setIntro3(0);
				break;
			case 3:
				help.setIntro4(7);
				break;
			case 4:
				INTRO_STAGE = -1;
				INTRO = false;
				break;
		}
	}

	public static void setTutorialStage(int stage) {
        TUTORIAL_STAGE = stage;
        switch(stage) {
            case 0: //plant seeds
                PoliceManager.SPAWNING_ACTIVE = false;
                Policeman.speedX = 8;
                World.player.setFlamethrowerActive(false);
                World.player.setLeafBlowerActive(false);
                help.setTutorial1(0);
                break;
            case 1: //harvest/seed
                help.setTutorial2(0);
                break;
            case 2: //equip flamethrower
                World.player.setFlamethrowerActive(true);
                help.setTutorial3a(0);
                break;
            case 3: //use flamethrower
                World.player.setLeafBlowerActive(true);
                help.setTutorial3b(0);
                break;
            case 4: //select blower
				PoliceManager.SPAWNING_ACTIVE = true;
				help.setTutorial4a(0);
                break;
            case 5: //use blower
                help.setTutorial4b(0);
                break;
			case 6: //repair field
				help.setTutorial5(0);
				break;
			case 7: //end tutorial
				help.setTutorialEnd(4);
				Policeman.speedX = Policeman.speedXMax;
				TUTORIAL = false;
				break;
        }
    }

	// Collision events --------------------------------
}

package com.gdx.game.gameplay;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.GdxGame;

public class PlantationPlants extends AnimatedSprite {

    private float growTime = 0;
    private String plantStates = GdxGame.ressources.imgPlantStates; //Names for the grow states
    private String plantStatesBurnt = GdxGame.ressources.imgPlantStatesBurnt;
    private float plantX;
    private float plantY;
    private float growPercentage = 0;
    private float growMax = 15; //scalable grow time
    private float scaleFactor = (1.5f*30 / growMax);

    private boolean isBurning = false;


    public PlantationPlants(float x, float y){
        super(); //constructor of animated sprite
        addAnimation(GdxGame.ressources.get(plantStates, Texture.class), 3, 1, 0, 1, 1, "Plant01", false);
        addAnimation(GdxGame.ressources.get(plantStates, Texture.class), 3, 1, 1, 1, 1, "Plant02", false);
        addAnimation(GdxGame.ressources.get(plantStates, Texture.class), 3, 1, 2, 1, 1, "Plant03", false);
        addAnimation(GdxGame.ressources.get(plantStatesBurnt, Texture.class), 3, 1, 0, 1, 1, "Plant01b", false);
        addAnimation(GdxGame.ressources.get(plantStatesBurnt, Texture.class), 3, 1, 1, 1, 1, "Plant02b", false);
        addAnimation(GdxGame.ressources.get(plantStatesBurnt, Texture.class), 3, 1, 2, 1, 1, "Plant03b", false);
        setAnimation("Plant01");
        plantX = x;
        plantY = y;
        setSize(70, 35);
        setOrigin(35, 0);
        setPosition(x, y);

        setScale(0);
        animate(0);
    }

    public void grow(){
        if (isBurning) {
            return;
        }
        if (growTime < 0.2333f*growMax) {
            setScale(0.02f*scaleFactor*growTime);
            for (int i = 1; i < 2; i++){
                scale(0.00001f*scaleFactor*i);}
            }
        else if (growTime >= 0.2333f*growMax && growTime < 0.3f*growMax) {
            scale(0.00001f*scaleFactor*growTime);
            for (int i = 1; i < 10; i++){
                scale(0.0000001f*scaleFactor*i);}
        }
        else if (growTime >= 0.3f*growMax && growTime < 0.4f*growMax) {
            setAnimation("Plant02");
            for (int i = 1; i < 10; i++){
                scale(0.000001f*scaleFactor*i);}
        }
        else if (growTime >= 0.4f*growMax && growTime < 0.6666f*growMax) {
            for (int i = 1; i < 10; i++){
                scale(0.000001f*scaleFactor*20*i);}
        }
        else if (growTime >= 0.6666f*growMax && growTime < 0.83f*growMax) {
            for (int i = 1; i < 10; i++) {
                scale(0.0001f * scaleFactor / (i));
            }
        }
        else if (growTime >= 0.83f*growMax && growTime < growMax){
            scale(0.00001f*growTime*scaleFactor);
        }
        else if (growTime >= growMax){
            setAnimation("Plant03");
            setScale(1.8f);
        }
        growPercentage = Math.min((growTime / growMax), 1);
    }

    public float getGrowPercentage() {
        return growPercentage;
    }


    public void update(float delta) {
        growTime += delta;
        grow();
        animate(delta);
    }

    public void draw(SpriteBatch sb) {
        // do da drawing ting
        super.draw(sb);
    }

    public void setBurning(boolean burning) {
        isBurning = burning;
        setAnimation(activeAnimationName.concat("b"));  // choose burnt animation
        // setScale(getScaleX() * 0.5f);
    }
}

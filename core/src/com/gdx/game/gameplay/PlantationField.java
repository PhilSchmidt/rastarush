package com.gdx.game.gameplay;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.GdxGame;
import com.gdx.game.gameplay.emitters.FireEmitter;
import com.gdx.game.gameplay.emitters.SmokeEmitter;
import com.gdx.game.gameplay.emitters.StarEmitter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class PlantationField extends AnimatedSprite{

    Plantation plantation;
    ArrayList<PlantationPlants> plants = new ArrayList<PlantationPlants>(); //List for the spawning plants
    private Sprite plantField;
    private int plantHeight = 100;
    private int plantWidth = 80;
    private int fireThresh = 15;
    public float starsTime = 0;
    private boolean burning = false;
    public FireEmitter fireEmitter;
    public SmokeEmitter smokeEmitter;
    public StarEmitter starsEmitter;
    private boolean isGrowing = false;
    private float starsDuration = 1;
    private int minDrops = 5;

    private float maxSmoke = 30.0f;
    //private boolean harvest = false;
    private float maxBurnTime = 10.0f;
    private float burnTime = maxBurnTime;

    public PlantationField(Plantation plantation, float a, float b) {
        this.plantation = plantation;
        plantField = new Sprite(GdxGame.ressources.get(GdxGame.ressources.imgPlantField, Texture.class));
        plantField.setPosition(a, b);
        plantField.setSize(plantWidth, plantHeight);

        fireEmitter = new FireEmitter(new Vector2(a + plantWidth / 2, b + plantHeight / 2),
                null, new Vector2(0, 1.0f), 0.7f);
        smokeEmitter = new SmokeEmitter(new Vector2(a + plantWidth / 2, b + plantHeight / 2), World.player);
        starsEmitter = new StarEmitter(new Vector2(a + plantWidth / 2, b + plantHeight / 1.5f));

        // planting(minDrops); //for debugging
    }

    public void handleWater(int numDrops) {
        if (numDrops < minDrops) {
            return;
        }
        if (isGrowing) {
            float percentage = plants.get(0).getGrowPercentage();
            if (percentage < 1) {
                return;
            }
            else {
                harvest();
            }
        }
        if (GameControl.TUTORIAL && GameControl.TUTORIAL_STAGE == 6)
            GameControl.setTutorialStage(GameControl.TUTORIAL_STAGE + 1);

        reset();
        planting();
    }

    public void harvest() {
        World.score++;

        plantation.startPulseHouse(0.2f, 0.7f, 2);

        if (GameControl.TUTORIAL && GameControl.TUTORIAL_STAGE == 1)
            GameControl.setTutorialStage(GameControl.TUTORIAL_STAGE + 1);
    }

    public void planting(){
        for (int i = 0; i <8; i++) {
            float x = (float) (plantWidth*0.2+Math.random()*plantWidth*0.6 + plantField.getX() - 35); //zw 100 and 200
            float y = (float) (Math.random()*plantHeight*0.7 + plantField.getY());
            plants.add(new PlantationPlants(x, y));}
        Collections.sort(plants, new Comparator<PlantationPlants>() {
            @Override
            public int compare(PlantationPlants first, PlantationPlants second) {
                if (first.getY() < second.getY()) {
                    return 1;
                }
                else if (first.getY() > second.getY()) {
                    return -1;
                }
                return 0;
            }
        });
        isGrowing = true;

        if (GameControl.TUTORIAL && GameControl.TUTORIAL_STAGE == 0)
            GameControl.setTutorialStage(GameControl.TUTORIAL_STAGE + 1);
    }

    public void reset(){
        //initiates when player harvest field or when field is burned down.
        plants.clear();
        isGrowing = false;
        smokeEmitter.stop();
        fireEmitter.stop();
        burning = false;
        burnTime = maxBurnTime;
        starsTime = 0;
        plantField.setColor(Color.WHITE);
    }

    public void starShower(float delta){
        if (isGrowing) {
            if (plants.get(0).getGrowPercentage() == 1 && !starsEmitter.isEmitting()){
                starsEmitter.start();
            }
        }
        if (starsEmitter.isEmitting()){
            starsTime += delta;
            if (starsDuration <= starsTime){
                starsEmitter.stop();
            }
        }
    }

    public void update(float delta) {
        for (PlantationPlants p: plants) {
            p.update(delta);
        }

        if (burning) {
            burnTime -= delta;
            if (burnTime < 0) {
                fireEmitter.stop();
                smokeEmitter.stop();
                burning = false;
                if (GameControl.TUTORIAL && (GameControl.TUTORIAL_STAGE == 4 || GameControl.TUTORIAL_STAGE == 5)) {
                    GameControl.help.setTutorialRetry(0);
                    GameControl.setTutorialStage(GameControl.TUTORIAL_STAGE -1);
                }
            }
        }

        starShower(delta);
        starsEmitter.update(delta);
        fireEmitter.update(delta);
        smokeEmitter.update(delta);
    }

    public void draw(SpriteBatch sb){
        // do da drawing ting
        plantField.draw(sb);
        for (PlantationPlants p : plants) {
            p.draw(sb);
        }
        starsEmitter.draw(sb);

        // called from World for correct draw order
        // fireEmitter.draw(sb);
        // smokeEmitter.draw(sb);
    }

    public Rectangle getRect() {
        return plantField.getBoundingRectangle();
    }

    public void makeItBunDem(int collisions) {
        if (collisions > fireThresh && isGrowing && !burning && burnTime > 0) {
            isGrowing = false;
            for (PlantationPlants p: plants) {
                p.setBurning(true);
            }

            fireEmitter.start();
            smokeEmitter.setParticleRate(plants.get(0).getGrowPercentage() * maxSmoke);
            smokeEmitter.start();
            plantField.setColor(0.5f, 0.5f, 0.5f, 1.0f);
            burning = true;

            if (GameControl.TUTORIAL && GameControl.TUTORIAL_STAGE == 3)
                GameControl.setTutorialStage(GameControl.TUTORIAL_STAGE + 1);
        }
    }
}


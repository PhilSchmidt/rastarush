package com.gdx.game.gameplay;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.GdxGame;
import com.gdx.game.gameplay.tools.FlameThrower;
import com.gdx.game.gameplay.tools.LeafBlower;
import com.gdx.game.gameplay.tools.Tool;
import com.gdx.game.gameplay.tools.WateringCan;

import static java.lang.Math.abs;

public class Player extends AnimatedSprite {

    //adjustable parameters
    private int movementBorderX = 100;
    private int movementBorderY = 100;
    private int width = 50;
    private int height = 70;
    private int startPosX = GdxGame.screenWidth / 2 - width / 2;
    private int startPosY = GdxGame.screenHeight / 2 - height / 2 - 100;
    public float speedX = 200.0f;
    public float speedY = 200.0f;
    public float speed = 200.0f;
    private String textureFrontPath = GdxGame.ressources.imgRastaFront;
    private String textureBackPath = GdxGame.ressources.imgRastaBack;
    private float angularSpeed = 180.0f;

    private float moveHorizontal = 0;
    private float moveVertical = 0;
    private int turning = 0;
    private float movementX;
    private float movementY;
    private float newPosX;
    private float newPosY;

    private LeafBlower leafBlower;
    public FlameThrower flameThrower;
    public WateringCan wateringCan;
    Tool[] tools;
    int activeToolIndex = 0;
    private Tool activeTool;


    public Player() {
        //load rastaman
        super();
        addAnimation(GdxGame.ressources.get(textureFrontPath, Texture.class), 1, 1, 0, 1, 1, "front", false);
        addAnimation(GdxGame.ressources.get(textureBackPath, Texture.class), 1, 1, 0, 1, 1, "back", false);
        setAnimation("front");
        setPosition(startPosX, startPosY);
        setSize(width, height);

        leafBlower = new LeafBlower(this);
        flameThrower = new FlameThrower(this);
        wateringCan = new WateringCan(this);

        tools = new Tool[]{wateringCan, flameThrower, leafBlower};
        activeTool = tools[activeToolIndex];

        activeTool.setVisibility(true);
    }

    public void update(float delta) {
        //make da rastaman move
        super.animate(delta);

        move(delta);

        if (GameControl.TUTORIAL) {
            if( GameControl.TUTORIAL_STAGE == 2) {
                if (activeTool == flameThrower) {
                    GameControl.setTutorialStage(GameControl.TUTORIAL_STAGE + 1);
                }
            } else if( GameControl.TUTORIAL_STAGE == 4) {
                if (activeTool == leafBlower) {
                    GameControl.setTutorialStage(GameControl.TUTORIAL_STAGE + 1);
                }
            }
        }

        for (Tool t: tools) {
            t.update(delta);
        }
    }

    private void move(float delta) {
        movementX = moveHorizontal * speedX * delta;
        movementY = moveVertical * speedY * delta;
        Vector2 moveVec = new Vector2(movementX, movementY);
        moveVec = moveVec.setLength(Math.min(moveVec.len(), speed * delta));
        movementX = moveVec.x;
        movementY = moveVec.y;
        newPosX = getX() + movementX;
        newPosY = getY() + movementY;
        if (newPosX > movementBorderX && newPosX < GdxGame.screenWidth - movementBorderX - getWidth())
            setPosition(newPosX, getY());

        if (newPosY > movementBorderY && newPosY < GdxGame.screenHeight - movementBorderY)
            setPosition(getX(), newPosY);

        float newAngle = activeTool.angle + turning * angularSpeed * delta;
        if (newAngle > 360) {
            newAngle -= 360;
        }
        else if (newAngle < 0) {
            newAngle += 360;
        }
        activeTool.setAngle(newAngle);
    }

    public void draw(SpriteBatch sb) {
        // do da drawing ting
        if (activeAnimationName.equals("front")) {
            super.draw(sb);
        }
        for (Tool t: tools) {
            t.draw(sb);
        }
        if (activeAnimationName.equals("back")) {
            super.draw(sb);
        }
    }

    public void useTool() {
        activeTool.setActive(true);
    }

    public void stopTool() {
        activeTool.setActive(false);
    }

    // Check eff oddah inside blow cone an calculate blow strength
    public Vector2 getBlowVec(Vector2 other) {
        return activeTool.getBlowVec(other);
    }

    // MOVEMENT --------------------------------

    public void goLeft() {
        moveHorizontal -= 1;
    }

    public void goRight() {
        moveHorizontal += 1;
    }

    public void goUp() {
        moveVertical += 1;
        setAnimation("back");
    }

    public void goDown() {
        moveVertical -= 1;
        setAnimation("front");
    }

    public void stopLeft() {
        moveHorizontal += 1;
    }

    public void stopRight() {
        moveHorizontal -= 1;
    }

    public void stopUp() {
        moveVertical -= 1;
    }

    public void stopDown() {
        moveVertical += 1;
    }

    public void turnLeft() {
        turning += 1;
    }

    public void turnRight() {
        turning -= 1;
    }

    public void stopTurnLeft() {
        turning -= 1;
    }

    public void stopTurnRight() {
        turning += 1;
    }

    public void controllerInputX(float x) {
        moveHorizontal = x;
    }

    public void controllerInputY(float y) {
        moveVertical = y;
    }

    public void controllerToolAngle(float degree) {
        for (Tool t: tools) {
            t.setAngle(degree);
        }
    }

    public void angleFromMouse(float x, float y) {
        Vector2 direction = new Vector2(x, y);
        direction.sub(activeTool.getBack());
        float angle = direction.angle();
        float angleFromX = angle < 90 ? angle - 90 : angle + 270;

        for (Tool t: tools) {
            t.setAngle(angleFromX);
        }
    }

    public void switchTool() {
        activeTool.setActive(false);
        activeTool.setVisibility(false);

        while(true) {
            activeToolIndex++;
            activeToolIndex %= tools.length;
            activeTool = tools[activeToolIndex];
            if (!activeTool.forbidden) {
                activeTool.setVisibility(true);
                break;
            }
        }
    }

    public void setWaterCanActive(boolean active) {
        wateringCan.forbidden = !active;
        if (activeTool == wateringCan && !active) {
            switchTool();
        }
    }

    public void setFlamethrowerActive(boolean active) {
        flameThrower.forbidden = !active;
        if (activeTool == flameThrower && !active) {
            switchTool();
        }
    }

    public void setLeafBlowerActive(boolean active) {
        leafBlower.forbidden = !active;
        if (activeTool == leafBlower && !active) {
            switchTool();
        }
    }
}

package com.gdx.game.gameplay;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.GdxGame;
import com.gdx.game.gameplay.emitters.SmokeEmitter;

import java.util.Random;

public class Policeman extends AnimatedSprite {

    public boolean dead;

    //adjustable parameters
    private int width = 60;
    private int height = 60;
    public static float speedX = 9.0f;
    public static float speedXMax = 9.0f;
    private float speedY = speedX;
    private String textureFrontPath = GdxGame.ressources.imgPoliceFront;
    private String textureBackPath = GdxGame.ressources.imgPoliceBack;
    private String textureDonutPath = GdxGame.ressources.imgDonut;
    private int spawnBorderOffset = 80;
    private int spawnRangeX = GdxGame.screenWidth - (2 * (int)World.plantation.getWholePlantingArea().getX());
    private int spawnRangeY = GdxGame.screenHeight - (2 * (int)World.plantation.getWholePlantingArea().getY());
    private int spawnPosLeft = spawnBorderOffset;
    private int spawnPosRight = GdxGame.screenWidth - (2 * spawnBorderOffset);
    private int spawnPosTop = GdxGame.screenHeight - (2 * spawnBorderOffset);
    private int spawnPosBottom = spawnBorderOffset;
    private int deathOffset = -120;

    private int spawnDirection;
    private int startPosX = 0;
    private int startPosY = 0;
    private int moveHorizontal = 0;
    private int moveVertical = 0;
    private float movementX;
    private float movementY;
    private float newPosX;
    private float newPosY;

    private boolean AIstage1;
    private boolean AIstage2;
    private boolean moveBack;

    private boolean hungry = false;
    private Sprite donut;
    private float donutOffsetX = -10.0f;
    private float donutOffsetY = -10.0f;

    private boolean burning = false;
    private float maxBurnTime = 3.0f;
    private float burnTime = 0.0f;
    private float burnCooldown;

    private float middleDistance = 40;
    private boolean dying = false;
    private float deathCooldown;
    private float deadSince;

    public SmokeEmitter smokeEmitter;
    public SmokeEmitter smokeEmitterBurn;

    public Policeman() {
        //load policeman animation
        addAnimation(GdxGame.ressources.get(textureFrontPath, Texture.class), 1, 1, 0, 1, 1, "front", false);
        addAnimation(GdxGame.ressources.get(textureBackPath, Texture.class), 1, 1, 0, 1, 1, "back", false);
        setAnimation("front");
        setSize(width, height);

        donut = new Sprite(GdxGame.ressources.get(textureDonutPath, Texture.class));
        donut.setSize(40, 40);

        smokeEmitter = new SmokeEmitter(new Vector2(0, 0), World.player);
        smokeEmitter.setParticleRate(3);
        smokeEmitter.emitter.effectDescriptor.radius = 2;
        deathCooldown = smokeEmitter.emitter.effectDescriptor.lifetime;

        smokeEmitterBurn = new SmokeEmitter(new Vector2(0, 0), World.player);
        smokeEmitterBurn.setParticleRate(15);
        smokeEmitterBurn.emitter.effectDescriptor.radius = 5;
        burnCooldown = maxBurnTime + smokeEmitterBurn.emitter.effectDescriptor.lifetime;

        spawnRandom();

        SoundManager.play(SoundManager.soundPoliceSpawn, 0.5f);
    }

    public void spawnRandom()
    {
        Random random = new Random(System.currentTimeMillis());

        spawnDirection = random.nextInt(4);
        switch (spawnDirection) {
            case 0: //left
                startPosX = spawnPosLeft;
                startPosY = (int)World.plantation.getWholePlantingArea().getY() + random.nextInt(spawnRangeY);
                goRight();
                break;
            case 1: //right
                startPosX = spawnPosRight;
                startPosY = (int)World.plantation.getWholePlantingArea().getY() + random.nextInt(spawnRangeY);
                goLeft();
                break;
            case 2: //top
                startPosX = (int)World.plantation.getWholePlantingArea().getX() + random.nextInt(spawnRangeX);
                startPosY = spawnPosTop;
                goDown();
                break;
            case 3: //bottom
                startPosX = (int)World.plantation.getWholePlantingArea().getX() + random.nextInt(spawnRangeX);
                startPosY = spawnPosBottom;
                goUp();
                break;
        }
        setPosition(startPosX, startPosY);
    }

    public void update(float delta) {
        //make da police move
        super.animate(delta);

        if (!burning) {
            if (dying) {
                deadSince += delta;
                if (deadSince > deathCooldown) {
                    dead = true;
                    return;
                }
            }
            move(delta);
            donut.setPosition(getX() + width + donutOffsetX, getY() + height + donutOffsetY);
            Vector2 posVec = new Vector2(getX() + width / 2, getY() + height / 2);
            smokeEmitter.setPosition(posVec);
            smokeEmitterBurn.setPosition(posVec);
        }
        else {
            burnTime += delta;
            if (burnTime > maxBurnTime) {
                if (burnTime > burnCooldown)
                    dead = true;
                World.score = World.score + (int) (100 + Math.random() * 25);
                smokeEmitterBurn.stop();
            }
            else if (burnTime / maxBurnTime < 0.75) {
                setColor(Color.WHITE.cpy().lerp(Color.BLACK, burnTime / (maxBurnTime * 0.75f)));
            }
            else {
                setAlpha(1.0f - ((burnTime / maxBurnTime - 0.75f) * 4.0f));
            }
        }
        smokeEmitter.update(delta);
        smokeEmitterBurn.update(delta);

    }

    public void burn() {
        burning = true;
        smokeEmitterBurn.start();
    }

    public void makeHungry() {
        hungry = true;
        setMoveBack();

        if (GameControl.TUTORIAL && GameControl.TUTORIAL_STAGE == 5)
            GameControl.setTutorialStage(GameControl.TUTORIAL_STAGE + 1);
    }

    private void die() {
        dying = true;
        smokeEmitter.stop();
        deadSince = 0;
        setAlpha(0);
        donut.setAlpha(0);
    }

    private void move(float delta) {
        if (!moveBack)
            movingAI();

        movementX = moveHorizontal * speedX * delta;
        movementY = moveVertical * speedY * delta;
        newPosX = getX() + movementX;
        newPosY = getY() + movementY;
        setPosition(newPosX, newPosY);

        //delete when out of screen
        if (moveBack && (getX() < -deathOffset || getX() > GdxGame.screenWidth + deathOffset
                || getY() > GdxGame.screenHeight + deathOffset || getY() < -deathOffset)) {
            die();
        }

        //die when in da house
        if (Math.abs(getX() + getWidth() / 2 - GdxGame.screenWidth / 2) < middleDistance && Math.abs(getY() +
                getHeight() / 2 - GdxGame.screenHeight / 2) < middleDistance) {
            World.policeArrived();
            makeHungry();
            smokeEmitter.start();
        }
    }

    private void setMoveBack() {
        if (!moveBack) {
            moveBack = true;
            SoundManager.play(SoundManager.soundPoliceHigh);

            stop();
            switch (spawnDirection) {
                case 0: //left
                    goLeft();
                    break;
                case 1: //right
                    goRight();
                    break;
                case 2: //top
                    goUp();
                    break;
                case 3: //bottom
                    goDown();
                    break;
            }
        }
    }
    private void movingAI() {
        if (!AIstage1) {
            //just arrived at plantation area
            if (getBoundingRectangle().overlaps(World.plantation.wholePlantingArea)){
                AIstage1 = true;
                if (spawnDirection == 0) { //left
                    stopRight();
                    if (getY() < GdxGame.screenHeight / 2) {
                        goUp();
                    } else if (getY() > GdxGame.screenHeight / 2) {
                        goDown();
                    }
                }
                else if (spawnDirection == 1) { //right
                    stopLeft();
                    if (getY() < GdxGame.screenHeight / 2) {
                        goUp();
                    }
                    else if (getY() > GdxGame.screenHeight / 2) {
                        goDown();
                    }
                }
                else if (spawnDirection == 2) { //top
                    stopDown();
                    if (getX() < GdxGame.screenWidth / 2) {
                        goRight();
                    } else if (getX() > GdxGame.screenWidth / 2) {
                        goLeft();
                    }
                }
                else if (spawnDirection == 3) { //bottom
                    stopUp();
                    if (getX() < GdxGame.screenWidth / 2) {
                        goRight();
                    } else if (getX() > GdxGame.screenWidth / 2) {
                        goLeft();
                    }
                }
            }

        }
        else { //AIstage1 = true
            if (!AIstage2) {
                //arrived at road to the house
                if (moveVertical > 0) {//going up
                    if (getY() > GdxGame.screenHeight / 2 - 15) { //arrived at center of screen
                        AIstage2 = true;
                        if (spawnDirection == 0) { //from left
                            stopUp();
                            goRight();
                        } else if (spawnDirection == 1) { //from right
                            stopUp();
                            goLeft();
                        }
                    }
                } else if (moveVertical < 0) {//going down
                    if (getY() < GdxGame.screenHeight / 2 - 15) { //arrived at center of screen
                        AIstage2 = true;
                        if (spawnDirection == 0) { //from left
                            stopDown();
                            goRight();
                        } else if (spawnDirection == 1) { //from right
                            stopDown();
                            goLeft();
                        }
                    }
                } else if (moveHorizontal < 0) {//going left
                    if (getX() < GdxGame.screenWidth / 2 - 25) { //arrived at center of screen
                        AIstage2 = true;
                        if (spawnDirection == 2) { //from top
                            stopLeft();
                            goDown();
                        } else if (spawnDirection == 3) { //from bottom
                            stopLeft();
                            goUp();
                        }
                    }
                } else if (moveHorizontal > 0) {//going right
                    if (getX() > GdxGame.screenWidth / 2 - 25) { //arrived at center of screen
                        AIstage2 = true;
                        if (spawnDirection == 2) { //from top
                            stopRight();
                            goDown();
                        } else if (spawnDirection == 3) { //from bottom
                            stopRight();
                            goUp();
                        }
                    }
                }
            }
        }
    }

    public void draw(SpriteBatch sb) {
        //draw animation
        super.draw(sb);
        if (hungry) {
            donut.draw(sb);
        }
    }



    // MOVEMENT --------------------------------

    public void goLeft() {
        setAnimation("front");
        moveHorizontal -= 1;
    }

    public void goRight() {
        setAnimation("front");
        moveHorizontal += 1;
    }

    public void goUp() {
        moveVertical += 1;
        setAnimation("back");
    }

    public void goDown() {
        moveVertical -= 1;
        setAnimation("front");
    }

    public void stop() {
        moveHorizontal = 0;
        moveVertical = 0;
    }
    public void stopLeft() {
        moveHorizontal += 1;
    }

    public void stopRight() {
        moveHorizontal -= 1;
    }

    public void stopUp() {
        moveVertical -= 1;
    }

    public void stopDown() {
        moveVertical += 1;
    }
}

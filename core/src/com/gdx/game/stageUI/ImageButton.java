package com.gdx.game.stageUI;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;


public class ImageButton extends Image {
    private TextureRegion textureDisabled;
    private TextureRegion textureEnabled;
    private TextureRegion textureEnabledPressed;

    public ImageButton(TextureRegion textureEnabled, TextureRegion texturePressed, TextureRegion textureDisabled) {
        super(textureEnabled);
        this.textureEnabled = textureEnabled;
        this.textureEnabledPressed = texturePressed;
        this.textureDisabled = textureDisabled;
        this.setTouchable(Touchable.enabled);
    }

    public ImageButton(TextureRegion textureEnabled) {
        super(textureEnabled);
        this.textureEnabled = textureEnabled;
    }

    public void setEnabled() {
        setTouchable(Touchable.enabled);
        setDrawable(new SpriteDrawable(new Sprite(textureEnabled)));
    }

    public void setPressed() {
        if (textureEnabledPressed != null)
            setDrawable(new SpriteDrawable(new Sprite(textureEnabledPressed)));
    }

    public void setDisabled() {
        setTouchable(Touchable.disabled);
        if (textureDisabled != null)
            setDrawable(new SpriteDrawable(new Sprite(textureDisabled)));
    }
}

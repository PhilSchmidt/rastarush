package com.gdx.game.stageUI;

import com.gdx.game.GameplayScreen;

public interface UIDialogueListener {
    public void handleDialogAction(GameplayScreen.UIAction action, int value);
}

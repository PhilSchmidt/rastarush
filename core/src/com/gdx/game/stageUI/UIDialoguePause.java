package com.gdx.game.stageUI;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.gdx.game.GameplayScreen;
import com.gdx.game.GdxGame;


public class UIDialoguePause extends UIDialogue {

    public UIDialoguePause(int ingamePauseButtonPosX, int ingamePauseButtonPosY) {
        float screenCenterX = (GdxGame.screenWidth / 2);
        float screenCenterY = (GdxGame.screenHeight / 2);
        float buttonOffsetY = 70;
        int buttonSize = 86;

        //Background
        Image imgBackground = new Image(new TextureRegion(
                GdxGame.ressources.get(GdxGame.ressources.imgDialoguePause, Texture.class), 512, 240));
        imgBackground.setPosition(
                screenCenterX - (imgBackground.getWidth() / 2),
                screenCenterY - (imgBackground.getHeight() / 2) + 90);
        this.actors.add(imgBackground);

        //PLAY AGAIN
        TextureRegion textureBtnPlay = new TextureRegion(
                GdxGame.ressources.get(GdxGame.ressources.imgButtons, Texture.class), 128, 0, buttonSize, buttonSize);
        TextureRegion textureBtnPlayPressed = new TextureRegion(
                GdxGame.ressources.get(GdxGame.ressources.imgButtons, Texture.class), 128, 128, buttonSize, buttonSize);
        final ImageButton btnPlay = new ImageButton(textureBtnPlay, textureBtnPlayPressed, null);
        btnPlay.setBounds(
                screenCenterX - (buttonSize / 2),
                screenCenterY - buttonOffsetY,
                buttonSize, buttonSize);
        btnPlay.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                btnPlay.setPressed();
                // must return true for touchUp event to occur
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                btnPlay.setEnabled();
                notifyListener(GameplayScreen.UIAction.RESTART_GAME, 0);
            }
        });
        btnPlay.setTouchable(Touchable.enabled);
        this.actors.add(btnPlay);

        //RESUME GAME
        TextureRegion textureBtnResume = new TextureRegion(
                GdxGame.ressources.get(GdxGame.ressources.imgButtons, Texture.class), 256, 0, buttonSize, buttonSize);
        TextureRegion textureBtnResumePressed = new TextureRegion(
                GdxGame.ressources.get(GdxGame.ressources.imgButtons, Texture.class), 256, 128, buttonSize, buttonSize);

        final ImageButton btnResume = new ImageButton(textureBtnResume, textureBtnResumePressed, null);
        btnResume.setBounds(
                screenCenterX + 90,
                screenCenterY - buttonOffsetY,
                buttonSize, buttonSize);
        btnResume.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                btnResume.setPressed();
                // must return true for touchUp event to occur
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                btnResume.setEnabled();
                notifyListener(GameplayScreen.UIAction.RESUME_GAME, 0);
            }
        });
        btnResume.setTouchable(Touchable.enabled);
        this.actors.add(btnResume);

        //MENU
        Image btnMenu = new Image(new TextureRegion(
                GdxGame.ressources.get(GdxGame.ressources.imgButtons, Texture.class), 0, 0, buttonSize, buttonSize));
        btnMenu.setBounds(
                screenCenterX - 95 - buttonSize,
                screenCenterY - buttonOffsetY,
                buttonSize, buttonSize);
        btnMenu.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                System.out.println("menu touched");
                // must return true for touchUp event to occur
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                notifyListener(GameplayScreen.UIAction.TO_MENU, 0);
            }
        });
        btnMenu.setTouchable(Touchable.enabled);
        this.actors.add(btnMenu);

        //PAUSE BUTTON (invisible on stage, visible on Ingame-UI)
        Image btnPause = new Image();
        btnPause.setBounds(ingamePauseButtonPosX, ingamePauseButtonPosY, buttonSize, buttonSize);
        btnPause.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                // must return true for touchUp event to occur
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                notifyListener(GameplayScreen.UIAction.RESUME_GAME, 0);
            }
        });
        btnPause.setTouchable(Touchable.enabled);
        this.actors.add(btnPause);
    }
}

package com.gdx.game.stageUI;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.gdx.game.GameplayScreen;
import com.gdx.game.GdxGame;

import java.util.HashMap;

public class UIDialogueGameover extends UIDialogue {

    private HashMap<String, Integer> stats;
    private Label lblScore;

    public UIDialogueGameover() {
        float screenCenterX =  (GdxGame.screenWidth / 2);
        float screenCenterY =  (GdxGame.screenHeight / 2);
        float buttonOffsetY = 110;
        int buttonSize = 86;

        //Background
        Image imgBackground = new Image(new TextureRegion(
                GdxGame.ressources.get(GdxGame.ressources.imgDialogueGameover, Texture.class), 512, 290));
        imgBackground.setPosition(
                screenCenterX - (imgBackground.getWidth() / 2) - 30,
                screenCenterY - (imgBackground.getHeight() / 2) + 90);
        this.actors.add(imgBackground);

        //PLAY AGAIN
        Image btnPlay = new Image(new TextureRegion(
                GdxGame.ressources.get(GdxGame.ressources.imgButtons, Texture.class), 256, 0, buttonSize, buttonSize));
        btnPlay.setBounds(
                screenCenterX + 90,
                screenCenterY - buttonOffsetY,
                buttonSize, buttonSize);
        btnPlay.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                System.out.println("play touched");
                // must return true for touchUp event to occur
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                notifyListener(GameplayScreen.UIAction.RESTART_GAME, 0);
            }
        });
        btnPlay.setTouchable(Touchable.enabled);
        this.actors.add(btnPlay);

        //MENU
        Image btnMenu = new Image(new TextureRegion(
                GdxGame.ressources.get(GdxGame.ressources.imgButtons, Texture.class), 0, 0, buttonSize, buttonSize));
        btnMenu.setBounds(
                screenCenterX - 250,
                screenCenterY - buttonOffsetY,
                buttonSize, buttonSize);
        btnMenu.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                System.out.println("menu touched");
                // must return true for touchUp event to occur
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                notifyListener(GameplayScreen.UIAction.TO_MENU, 0);
            }
        });
        btnMenu.setTouchable(Touchable.enabled);
        this.actors.add(btnMenu);

        //SCORE
        //Label for score
        lblScore = new Label("Plants: 0",
                new Label.LabelStyle(GdxGame.ressources.fontPoints, Color.BLACK));
        lblScore.setPosition(
                screenCenterX - 50, screenCenterY + 75, Align.center);
        this.actors.add(lblScore);
    }

    public void load(Stage stage, HashMap<String, Integer> stats) {
        super.load(stage);
        this.stats = stats;

        lblScore.setText("Plants:  " + Integer.toString(stats.get("points")));
    }
}

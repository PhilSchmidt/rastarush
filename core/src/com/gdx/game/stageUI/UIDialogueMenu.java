package com.gdx.game.stageUI;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.gdx.game.GameplayScreen;
import com.gdx.game.helper.Highscore;
import com.gdx.game.GdxGame;

public class UIDialogueMenu extends UIDialogue {

    Label lblHighscore;

    public UIDialogueMenu () {

        //PRESS START
        Label lblPressStart = new Label("PRESS TO START",
                new Label.LabelStyle(GdxGame.ressources.fontPoints, Color.BLACK));
        lblPressStart.setPosition(
                GdxGame.screenWidth / 2, (GdxGame.screenHeight / 2) - 100, Align.center);
        this.actors.add(lblPressStart);

        //HIGHSCORE
        float highscore = this.loadHighscore();

        lblHighscore = new Label(
                Integer.toString((int)Math.round(highscore)),
                new Label.LabelStyle(GdxGame.ressources.fontPoints, Color.WHITE));
        lblHighscore.setBounds((GdxGame.screenWidth / 2) - 200, 100, 200, 30);

        this.actors.add(lblHighscore);

        //Invisible actor for touching
        Actor touchSpace = new Actor();
        //Fill screen, but let space for user to touch to show Android Buttons/Notification bar
        touchSpace.setBounds(0, 0, GdxGame.screenWidth - 90, GdxGame.screenHeight - 90);
        touchSpace.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                System.out.println("menu touched");
                notifyListener(GameplayScreen.UIAction.START_GAME, 0);
                // must return true for touchUp event to occur
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }
        });
        this.actors.add(touchSpace);
    }

    public void updateData() {
        this.lblHighscore.setText(Integer.toString((int)Math.round(loadHighscore())));
    }

    /**
     * Loads the best value from the highscore handler
     * @return the highest score
     */
    private float loadHighscore() {
        Highscore h = GdxGame.highscoreHandler.getHighscore();
        return h.getHighestScore();
    }
}

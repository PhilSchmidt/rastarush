package com.gdx.game.stageUI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.gdx.game.GameplayScreen;

import java.util.ArrayList;

public class UIDialogue {
    UIDialogueListener listener;
    Stage stage;
    ArrayList<Actor> actors;

    public UIDialogue() {
        actors = new ArrayList<Actor>();
    }

    public void setListener(UIDialogueListener listener) {
        this.listener = listener;
    }

    public void load(Stage stage) {
        this.stage = stage;
        stage.clear();
        for (int i=0; i< actors.size(); ++i) {
            stage.addActor(actors.get(i));
        }
        Gdx.input.setInputProcessor(stage);
    }

    public void draw(float delta){
        if (stage != null) {
            stage.act(delta);
            stage.draw();
        }
    }

    protected void notifyListener(GameplayScreen.UIAction action, int value) {
        if (listener != null)
            listener.handleDialogAction(action, value);
    }
}

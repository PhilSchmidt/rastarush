package com.gdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.gdx.game.gameplay.SoundManager;
import com.gdx.game.gameplay.World;
import com.gdx.game.stageUI.ImageButton;

/**
 * Created by schmi on 25.07.2017.
 */
public class MenuScreen implements Screen {

    public GdxGame game;

    private Stage stage;

    public MenuScreen(GdxGame game){this.game = game;}

    public void init() {
        StretchViewport viewport = new StretchViewport(game.screenWidth, game.screenHeight);
        stage = new Stage(viewport);

        //BACKGROUND
        Image background = new Image(GdxGame.ressources.get(GdxGame.ressources.imgBackgroundMenu, Texture.class));
        background.setSize(GdxGame.screenWidth, GdxGame.screenHeight);
        stage.addActor(background);


        Image logo = new Image(GdxGame.ressources.get(GdxGame.ressources.imgMenuLogo, Texture.class));
        logo.setPosition(370, 290);
        stage.addActor(logo);

        Image threeLittleBirds = new Image(GdxGame.ressources.get(GdxGame.ressources.imgThreeLittleBirds, Texture.class));
        threeLittleBirds.setPosition(800, 0);
        threeLittleBirds.setScale(0.66f);
        stage.addActor(threeLittleBirds);

        //LABEL: PRESS START
//        Label lblPressStart = new Label("RASTA RUSH",
//                new Label.LabelStyle(GdxGame.ressources.fontPoints, Color.WHITE));
//        lblPressStart.setPosition(
//                GdxGame.screenWidth / 2, (GdxGame.screenHeight / 2) - 100, Align.center);
//        stage.addActor(lblPressStart);

        //PLAY AGAIN
        float screenCenterX = (GdxGame.screenWidth / 2);
        float screenCenterY = (GdxGame.screenHeight / 2);
        float buttonOffsetY = 150;
        int buttonSize = 86;
        TextureRegion textureBtnResume = new TextureRegion(
                GdxGame.ressources.get(GdxGame.ressources.imgButtons, Texture.class), 256, 0, buttonSize, buttonSize);
        TextureRegion textureBtnResumePressed = new TextureRegion(
                GdxGame.ressources.get(GdxGame.ressources.imgButtons, Texture.class), 256, 128, buttonSize, buttonSize);

        final ImageButton btnResume = new ImageButton(textureBtnResume, textureBtnResumePressed, null);
        btnResume.setBounds(
                screenCenterX - 43,
                screenCenterY - buttonOffsetY,
                buttonSize, buttonSize);
        btnResume.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                btnResume.setPressed();
                // must return true for touchUp event to occur
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                btnResume.setEnabled();

                // --- START GAME ! ---
                game.gameplayScreen.init();
                game.setScreen(game.gameplayScreen);
            }
        });
        btnResume.setTouchable(Touchable.enabled);
        stage.addActor(btnResume);

        SoundManager.play(SoundManager.musicMenuIntro);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (stage != null) {
            stage.act(delta);
            stage.draw();
        }

        if (!SoundManager.musicMenuIntro.isPlaying() && !SoundManager.musicMenuLoop.isPlaying())
            SoundManager.play(SoundManager.musicMenuLoop);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}

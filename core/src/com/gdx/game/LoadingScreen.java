package com.gdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.StretchViewport;


public class LoadingScreen implements Screen {

	private GdxGame game;
	private Stage stage;
	private Image image;
	private String backgroundPath = "ui/loading.png";
	
	// constructor to keep a reference to the main Game class
	public LoadingScreen(GdxGame app){
		this.game = app;
		init();
	}
	
	private void init() {
		StretchViewport viewport = new StretchViewport(
				GdxGame.screenWidth, GdxGame.screenHeight);
		stage = new Stage(viewport);

		//Bild mittig setzen
    	image = new Image(new Texture(Gdx.files.internal( backgroundPath )));
    	image.setPosition(
			(GdxGame.screenWidth / 2) - (image.getWidth() / 2),
			(GdxGame.screenHeight / 2) - (image.getHeight() / 2));
    	stage.addActor(image);
	}
	
	@Override
	public void render(float delta) {
		if (GdxGame.ressources.update()) {
			GdxGame.ressources.initLoadedAssets();
			game.menuScreen.init();
			game.setScreen( game.menuScreen );
			System.out.println("------------ finished loading!");
		}
		//Hintergrundfarbe setzen
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		//Lade-Bild rendern
        stage.act(delta);
        stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		//anpassung an Bildschirmgröße (streckung, pressung)
		stage.getViewport().update(width, height, false);
	}

	@Override
	public void show() {
		stage.addAction(Actions.sequence( Actions.fadeOut(0.0f),Actions.fadeIn(0.5f))); 			 	
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
	}

	@Override
	public void dispose() {
		stage.dispose();		
	}
}

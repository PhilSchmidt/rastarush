package com.gdx.game;

import com.badlogic.gdx.audio.Sound;
import com.gdx.game.gameplay.SoundManager;
import com.gdx.game.helper.*;
import com.badlogic.gdx.Game;

/**
 * Loads Screens and Assets
 */
public class GdxGame extends Game {

	public static final int HTML_DEPLOYMENT = 1;
	//Screen settings
	public static int screenWidth = 1280;
	public static int screenHeight = 720;
	public static float ASPECT_RATIO = (float)screenWidth/(float)screenHeight;
	//Highscore values
	public static final String SAVE_NAME = "save";
	public static final String HIGHSCORE_FILE = "highscore.json";
	//Ressource manager
	public static RessourcesLoader ressources;
	public static SoundManager soundManager;
	//Class with all loaded values (saves, world, player) for the gameplay
	public static Values values;
	//Highscore manager
	public static HighscoreHandler highscoreHandler;
	//Screens
	public LoadingScreen loadingScreen;
	public GameplayScreen gameplayScreen;
	public MenuScreen menuScreen;

	@Override
	public void create() {
		//Lädt alle Assets mithilfe des Assetmanagers
		ressources = new RessourcesLoader();
		//Lädt und speichert alle Spielwerte
		values = new Values();
		//Bietet Funktionen zum Laden/Speichern von Highscores
		highscoreHandler = new HighscoreHandler();

		//Saves laden
		values.loadGameValues();
		//Initialen Start vermerken
		if (values.firstStart)
			System.out.println("Initial start!");

		//Screens laden
		loadingScreen 	= new LoadingScreen(this);
		gameplayScreen 	= new GameplayScreen(this);
		menuScreen 	= new MenuScreen(this);

		soundManager = new SoundManager();

		//Ressources laden, während der LoadingScreen angezeigt wird
		ressources.loadAssets();
		//LoadingScreen setzen
		setScreen(loadingScreen);
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
		ressources.dispose();
	}
}

package com.gdx.game;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.gdx.game.gameplay.GameControl;
import com.gdx.game.gameplay.SoundManager;
import com.gdx.game.gameplay.UI;
import com.gdx.game.helper.Highscore;
import com.gdx.game.stageUI.UIDialogue;
import com.gdx.game.stageUI.UIDialogueGameover;
import com.gdx.game.stageUI.UIDialogueListener;
import com.gdx.game.stageUI.UIDialoguePause;

/**
 * Showing the gamelpay controlled by GameControl class.
 * Shwoing Dialogs (Menu, Pause, Gameover) depending on GameControls state and user interaction
 */
public class GameplayScreen implements Screen, UIDialogueListener{

	public enum UIAction {START_GAME, RESTART_GAME, RESUME_GAME, TO_MENU}

	public GdxGame game;

	public float gameSpeed = 1.0f;
	public static float ratioX = 1.0f;
	public static float ratioY = 1.0f;

	public static boolean PLAYED_ONE_TIME = false;

	private GameControl gameControl;
	private UI ui;
	private SpriteBatch spriteBatch;
	private Stage uiStage;
	private UIDialogue activeDialogue;
	private UIDialogueGameover gameoverDialogue;
	private UIDialoguePause pauseDialogue;
	private boolean isResetGameplay;
	private boolean startAfterReset;
	private boolean isGameoverHandled;

	/**
	 constructor to keep a reference to the main Game class
  	*/
	public GameplayScreen(GdxGame game){
		this.game = game;
	}
	
	public void init() {
		//Spritebatch for gameworld
		spriteBatch = new SpriteBatch();
		//Stage for UI
		StretchViewport viewport = new StretchViewport(game.screenWidth, game.screenHeight);
		uiStage = new Stage(viewport);
		//Initialize all UIs and gameplay
		initGameoverUI();
		initPauseUI();
		initGameplay(new HashMap<String, Integer>(), new HashMap<String, Integer>());
		initGameUI();

		startGameplay();
	}
	
	@Override
	/**
	 * Rendering und Update
	 */
	public void render(float delta) {

		//UPDATE
		updateGameplay(delta);

		//RENDERING
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		spriteBatch.begin();

			//draw gameplay
			if (gameControl != null)
				gameControl.draw(spriteBatch);
			//Draw ingame-UI
			if (ui != null)
				ui.draw(spriteBatch);

		spriteBatch.end();

		//update ingame-UI and Dialog-UI
		updateUI(delta);

		if (isResetGameplay)
			this.resetGameplay(startAfterReset);

	}

	private void updateGameplay(float delta){
		if (gameControl != null) {
			//UPDATE GAMEPLAY
			gameControl.update(delta * gameSpeed);
			//handle gameover
			if (gameControl.isGameover() && !isGameoverHandled){
				HashMap<String, Integer> stats = gameControl.getGameStats();
				this.saveScore( stats.get("points") );
				GdxGame.values.saveGameValues();
				hideIngameUI();
				loadGameoverUI(stats);
				isGameoverHandled = true;
			}
		}
	}

	private void updateUI(float delta) {
		if (ui != null)
			ui.update(delta);
		if (activeDialogue != null)
			activeDialogue.draw(delta);
	}

	private void initGameoverUI() {
		gameoverDialogue = new UIDialogueGameover();
		gameoverDialogue.setListener(this);
	}

	private void initPauseUI(){
		pauseDialogue = new UIDialoguePause(GdxGame.screenWidth - 100, GdxGame.screenHeight - 100);
		pauseDialogue.setListener(this);
	}

	private void initGameUI() {
		ui = new UI(this, gameControl);
		ui.hide();
	}

	private void initGameplay(Map<String, Integer> worldSettings, Map<String, Integer> playerSettings) {
		gameControl = new GameControl();
		gameControl.load(worldSettings, playerSettings);
	}

	private void loadGameoverUI(HashMap<String, Integer> stats) {
		gameoverDialogue.load(uiStage, stats);
		activeDialogue = gameoverDialogue;
	}

	public void loadPauseUI() {
		gameControl.pause();
		GdxGame.values.saveGameValues();
		pauseDialogue.load(uiStage);
		activeDialogue = pauseDialogue;
	}

	private void loadPlayerValues() {
		//load player values from settings
	}

	private void loadIngameUI() {
		ui.show();
		Gdx.input.setInputProcessor(ui);
	}

	private void hideIngameUI() {
		ui.hide();
	}

	private void hidePauseUI() { this.activeDialogue = null; }

	private void startGameplay() {
		PLAYED_ONE_TIME = true;
		loadPlayerValues();
		loadIngameUI();
		this.isGameoverHandled = false;
		this.gameControl.start();
	}

	private void resetGameplay(boolean start) {
		SoundManager.musicIngameIntro.stop();
		SoundManager.musicIngameLoop.stop();
		gameControl = null;
		this.initGameplay(new HashMap<String, Integer>(), new HashMap<String, Integer>());
		this.initGameUI();
		isResetGameplay = false;

		if (start)
			this.startGameplay();
		else {
			hidePauseUI();
			game.setScreen(game.menuScreen);
		}
	}

	private void resumeGame() {
		this.hidePauseUI();
		Gdx.input.setInputProcessor(ui);
		gameControl.resume();
	}


	@Override
	public void handleDialogAction(UIAction action, int value) {
		if (action == UIAction.START_GAME) {
			this.startGameplay();
		}
		else if (action == UIAction.RESTART_GAME) {
			this.isResetGameplay = true;
			this.startAfterReset = true;
			this.hidePauseUI();
		}
		else if (action == UIAction.TO_MENU) {
			this.isResetGameplay = true;
			this.startAfterReset = false;
			GdxGame.values.saveGameValues();
		}
		else if (action == UIAction.RESUME_GAME) {
			this.resumeGame();
		}
		else {
			System.out.println("GameplayScreen: Unknown Dialog Action!");
		}
	}

	@Override
	public void resize(int width, int height) {
		//stretch/crop on a bigger/smaller screen resolution
		spriteBatch.getProjectionMatrix().setToOrtho2D(0, 0, GdxGame.screenWidth, GdxGame.screenHeight);
		//get the ratio to stretch/crop on a different resolution
		ratioX = GdxGame.screenWidth /  Gdx.graphics.getWidth() ;
		ratioY = GdxGame.screenHeight / Gdx.graphics.getHeight();
	}

	@Override
	public void show() {
		Controllers.addListener(ui);
		if (gameControl != null)
			gameControl.resume();	
	}

	@Override
	public void hide() {
		GdxGame.values.saveGameValues();
		if (gameControl != null)
			gameControl.pause();
		
		Controllers.removeListener(ui);
	}

	@Override
	public void pause() {
		GdxGame.values.saveGameValues();		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub	
	}

	@Override
	public void dispose() {
		GdxGame.values.saveGameValues();
		spriteBatch.dispose();
	}
	
	private void saveScore(int score) {
		Highscore h = GdxGame.highscoreHandler.getHighscore();
		h.put(score);
		GdxGame.highscoreHandler.saveHighscore(h);
	}
}

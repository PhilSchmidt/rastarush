package com.gdx.game.helper;

import java.util.ArrayList;
import java.util.Collections;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Json.Serializable;
import com.badlogic.gdx.utils.JsonValue;

/**
 * Objekt zum sortierten Speichern mehrerer Scores.
 * Serializable
 * 
 * @author Philip
 *
 */
public class Highscore implements Serializable{

	public static int MAX_HIGHSCORES = 10;
	/*
	 * Alle Highscore-Eintr�ge; Float, da JSON integer als float interpretiert
	 */
	ArrayList<Float> highscores;
	
	public Highscore() {
		highscores = new ArrayList<Float>();
	}
	
	@Override
	public void write(Json json) {
		json.writeValue( "highscores", highscores );
		System.out.println("Highscore write success");
	}
	
	@Override
	public void read(Json json, JsonValue jsonData) {
		//als hashmap auslesen, da TreeMap nicht funktioniert
		ArrayList<Float> scores = json.readValue( "highscores", ArrayList.class, jsonData );
		this.highscores = scores;
		System.out.println("Highscore read success");
	}
	
	public void put(int score) {
		if (highscores.size() == MAX_HIGHSCORES) {
			Collections.sort(highscores);
			if (score > highscores.get(0)) {
				highscores.add(0, (float)score);
			}
		} else {	
			this.highscores.add((float)score);
		}
		
	}
	
	public ArrayList<Float> getScores() {
		if (this.highscores == null){
			highscores = new ArrayList<Float>();
		} else {
			//Sorting
			Collections.sort(highscores);
		}
		
		return this.highscores;
	}

	public float getHighestScore() {
		if (this.highscores == null || this.highscores.isEmpty()){
			return 0.0f;
		} else {
			//Sorting
			Collections.sort(highscores);
			return highscores.get(highscores.size() - 1);
		}	
	}
	
}

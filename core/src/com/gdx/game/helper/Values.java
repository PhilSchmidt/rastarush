package com.gdx.game.helper;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.gdx.game.GdxGame;

/**
 * H�lt alle ben�tigten Gameplay-, Achievements-, Upgrade- 
 * und Spielerwerte in Variablen vor
 * 
 * @author Philip
 *
 */
public class Values {

	Highscore highscore;

	public boolean firstStart;
	
	/**
	 * Konstruktor lädt Startwerte
	 */
	public Values() {
		this.loadInitialValues();
	}
	
	public void loadInitialValues() {

	}
	
	public void loadGameValues() {
		System.out.println("loadGameValues");
		Preferences prefs = Gdx.app.getPreferences(GdxGame.SAVE_NAME);
		
		firstStart = prefs.getInteger("started") == 0;

		if (firstStart) {

			prefs.putInteger("started", 1);
			prefs.flush();
		}
		System.out.println("loaded");

	}
	
	public void saveGameValues() {
		System.out.println("saveGameValues");

		Preferences prefs = Gdx.app.getPreferences(GdxGame.SAVE_NAME);
		
		//prefs.putInteger("points", experience);
		
		prefs.flush();
	}
	
	public void savePlayerAttribute(String attr, int intValue) {
		System.out.println("savePlayerAttribute");

		Preferences prefs = Gdx.app.getPreferences(GdxGame.SAVE_NAME);
		prefs.putInteger(attr, intValue);
		prefs.flush();
	}
	
	public void savePlayerAttribute(String attr, float floatValue) {
		Preferences prefs = Gdx.app.getPreferences(GdxGame.SAVE_NAME);
		prefs.putFloat(attr, floatValue);
		prefs.flush();
	}

	public void resetPlayerSettings() {
		Preferences prefs = Gdx.app.getPreferences(GdxGame.SAVE_NAME);
		prefs.clear();
		prefs.flush();
	}
}

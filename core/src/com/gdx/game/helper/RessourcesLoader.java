package com.gdx.game.helper;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.BitmapFontLoader.BitmapFontParameter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.gdx.game.GdxGame;

/**
 * L�dt Bilder und Fonts aus dem Verzeichnissen "ui/" und "data/" 
 * und speichert diese als Texturen im AssetManager
 * 
 * @author Philip
 *
 */
public class RessourcesLoader extends AssetManager {

	// -------- PATHS TO ASSETS --------
	//UI
	public String imgDialogueGameover = "ui/dialogue_gameover.png";
	public String imgDialoguePause = "ui/dialogue_pause.png";
	public String imgButtons = "ui/ui_buttons.png";
	//BMP-Fonts
	public String fontPathShadow = "fonts/game_gabriola_shadow.fnt";
	public String fontPathButtons = "fonts/ysofc.fnt";
	public String fntBangers= "fonts/Bangers_bitmap.fnt";
	public String fntBangers2= "fonts/font2/Bangers_bitmap.fnt";
	public String fntBangers3= "fonts/font3/Bangers_bitmap.fnt";
	//TTFs
	public String ttfBangers= "fonts/Bangers.ttf";
	//World images
	public String imgRastaFront= "images/rastaman_front.png";
	public String imgRastaBack= "images/rastaman_back.png";
	public String imgYoung = "images/young.png";
	public String imgSmokeParticle = "images/smoke2.png";
	public String imgFireParticle = "images/fire.png";
	public String imgBird = "images/Bird.png";
	public String imgPoliceFront = "images/policeman_front.png";
	public String imgPoliceBack = "images/policeman_back.png";
	public String imgBackground = "images/background.png";
	public String imgBackgroundMenu = "images/backgroundMenu.png";
	public String imgLeafBlower = "images/blower.png";
	public String imgPlantStates = "images/Plant_States.png";
	public String imgPlantStatesBurnt = "images/Plant_States_Burnt.png";
	public String imgPlantField = "images/pantation_field.png";
	public String imgHouse = "images/house.png";
	public String imgThreeLittleBirds = "images/ThreeLittleBirds.png";
	public String imgStar = "images/star.png";
	public String imgDonut = "images/donut.png";
	public String imgMenuLogo = "images/Logo.png";
	public String imgWateringCan = "images/wateringCan.png";
	public String imgDrop = "images/drop.png";
	public String imgFlamethrower = "images/flamethrower.png";

    public String whiteRect = "images/white.png";
    public String triangle = "images/triangle.png";

	// ---------------------------------

	//Loaded TTF-Fonts
	public BitmapFont fontHeadlines;
	public BitmapFont fontPoints;
    public BitmapFont fontSpeechBubbles;

	public RessourcesLoader () {}

	public void loadAssets() {
		//Filter zur BitmapFont Darstellung setzen
		BitmapFontParameter fontParam = new BitmapFontParameter();
		fontParam.magFilter = Texture.TextureFilter.Linear;
		fontParam.minFilter = Texture.TextureFilter.Linear;
		//BitmapFonts laden
		load(fontPathShadow, BitmapFont.class, fontParam);
		load(fontPathButtons, BitmapFont.class, fontParam);
		load(fntBangers, BitmapFont.class, fontParam);
		load(fntBangers2, BitmapFont.class, fontParam);
		load(fntBangers3, BitmapFont.class, fontParam);

		//GUI Bilder laden
		load(imgDialogueGameover, Texture.class);
		load(imgDialoguePause, Texture.class);
		load(imgButtons, Texture.class);

		//Gameplay Bilder laden
		load(imgRastaFront, Texture.class);
		load(imgRastaBack, Texture.class);
		load(imgSmokeParticle, Texture.class);
		load(imgFireParticle, Texture.class);
		load(imgYoung, Texture.class);
		load(imgBird, Texture.class);
		load(imgPoliceFront, Texture.class);
		load(imgPoliceBack, Texture.class);
		load(imgBackground, Texture.class);
		load(imgBackgroundMenu, Texture.class);
		load(imgLeafBlower, Texture.class);
		load(imgPlantStates, Texture.class);
		load(imgPlantStatesBurnt, Texture.class);
		load(imgHouse, Texture.class);
		load(imgThreeLittleBirds, Texture.class);
		load(imgPlantField, Texture.class);
		load(imgStar, Texture.class);
		load(imgDonut, Texture.class);
		load(whiteRect, Texture.class);
		load(triangle, Texture.class);
		load(imgMenuLogo, Texture.class);
		load(imgWateringCan, Texture.class);
		load(imgDrop, Texture.class);
		load(imgFlamethrower, Texture.class);
	}

	public void initLoadedAssets() {
		loadFonts();
	}

    private void loadFonts() {
		if (GdxGame.HTML_DEPLOYMENT == 0) {
			////// for DESKTOP, ANDROID deployment ----------------------------------
			fontHeadlines = loadTrueTypeFont(
					this.ttfBangers, 120, Color.BLACK, 2, Color.WHITE);
			fontPoints = this.loadTrueTypeFont(
					this.ttfBangers, 50, Color.BLACK, 0, Color.WHITE);
			//fontSpeechBubbles = this.loadTrueTypeFont(
			//		this.fntBangers2, 25, Color.BLACK, 0, Color.WHITE);
		}
		else {
			////// for HTML demployment ---------------------------------------------
			fontHeadlines = get(fntBangers, BitmapFont.class);
			fontPoints = get(fntBangers, BitmapFont.class);
			fontHeadlines.setColor(Color.BLUE);
			fontPoints.setColor(Color.BLUE);
			fontSpeechBubbles = get(fntBangers2, BitmapFont.class);
			fontSpeechBubbles.getData().setScale(0.66f);

		}
	}

    public BitmapFont loadTrueTypeFont(
    		String file, int size, Color fontColor, int borderSize, Color borderColor) {
		BitmapFont font = new BitmapFont();

		////// REMOVE FOR HTML DEPLOY !
//		FreeTypeFontGenerator generator =
//				new FreeTypeFontGenerator(Gdx.files.internal(file));
//		FreeTypeFontGenerator.FreeTypeFontParameter parameter =
//				new FreeTypeFontGenerator.FreeTypeFontParameter();
//		parameter.size = size;
//		parameter.color = fontColor;
//		parameter.borderWidth = borderSize;
//		parameter.borderColor = borderColor;
//		font = generator.generateFont(parameter);
//		generator.dispose();
		//////

		return font;
	}
}

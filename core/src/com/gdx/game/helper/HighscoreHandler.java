package com.gdx.game.helper;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Base64Coder;
import com.badlogic.gdx.utils.Json;
import com.gdx.game.GdxGame;

/**
 * Bietet Fuktionen zum Laden und Speichern des Highscores.
 * Die Datei mit dem Highscore-Objekt wird verschl�sselt.
 * 
 * @author Philip
 *
 */
public class HighscoreHandler {
	
	/**
	 * L�dt verschl�sselten Highscore und gibt diesen in einem Highscore-Objekt zur�ck.
	 * Statische Variable HIGHSCORE_FILE wird zum Auslesen des Pfades ben�tigt
	 * @return Highscore
	 */
    public Highscore getHighscore() {
        Highscore highscore;
    	highscore = new Highscore();
    	
        // create the handle for the profile data file
        FileHandle dataFile = Gdx.files.local( GdxGame.HIGHSCORE_FILE );

        // create the JSON utility object
        Json json = new Json();

        // check if the profile data file exists
        if( dataFile.exists() ) {

            // load the profile from the data file
            try {

                // read the file as text
                String asCode = dataFile.readString();

                // decode the contents
                String asText = Base64Coder.decodeString( asCode );
                
                // restore the state
                highscore = json.fromJson( Highscore.class, asText );
                
            } catch( Exception e ) {
            	System.out.println("Highscore-Datei kann nicht gelesen werden!");
            }

        } else {
            // create a new profile data file
            saveHighscore( highscore );
        }

        // return the result
        return highscore;
    }
    
    /**
     * Speichert Highscore-Objekt verschl�sselt in einer Datei.
     * Statische Variable HIGHSCORE_FILE wird zum Auslesen des Pfades ben�tigt.
     * @param highscore
     */
    public void saveHighscore(Highscore highscore) {
        // create the JSON utility object
        Json json = new Json();

        // create the handle for the profile data file
        FileHandle dataFile = Gdx.files.local( GdxGame.HIGHSCORE_FILE );

        // convert the given profile to text
        String asText = json.toJson( highscore );

        // encode the text
        String asCode = Base64Coder.encodeString( asText );

        // write the profile data file
        dataFile.writeString( asCode, false );   	
    }
}
